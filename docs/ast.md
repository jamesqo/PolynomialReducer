# AST Docs

WIP

## FAQ

### How do I convert my `ExpressionNode` into a `Polynomial`?

There are several ways to do this, depending on your use case.

- If you know that your `ExpressionNode` is a standard expression, then just call `ToPolynomial()`. However, this will throw if your expression is not standard.

- If your expression isn't standard, then there's no way to convert it to a `Polynomial` (`1 / (x + 1)` can't be represented as a polynomial with integer coefficients). However, you can convert it to a `SimplePolynomial`, which permits simple fractions in its terms.
  - If you know the expression is already simple, call `ToSimplePolynomial(throwIfNotSimple: true)`.
  - Otherwise, call `ToSimplePolynomial(throwIfNotSimple: false)`. Your expression will be automatically simplified if necessary before being converted to `SimplePolynomial`.
