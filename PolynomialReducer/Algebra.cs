﻿using System;
using System.Collections.Generic;
using System.Text;
using PolynomialReducer.Internal.FrontEnd;
using PolynomialReducer.Nodes;

namespace PolynomialReducer
{
    public static class Algebra
    {
        public static EquationNode ParseEquation(string text) => CreateParser(text).ReadEquation();

        public static ExpressionNode ParseExpression(string text) => CreateParser(text).ReadExpression();

        private static AlgebraParser CreateParser(string text) => new AlgebraParser(text);
    }
}
