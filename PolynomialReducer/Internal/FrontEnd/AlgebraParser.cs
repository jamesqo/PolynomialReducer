﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Text;
using PolynomialReducer.Nodes;
using PolynomialReducer.Internal.Tokens;
using static PolynomialReducer.Nodes.NodeFactory;

namespace PolynomialReducer.Internal.FrontEnd
{
    // TODO: Consider banning constant factors aside from the first in a product.
    // e.g. 'x2' should not be allowed to parse to 2 * x.
    [DebuggerDisplay(nameof(CurrentToken) + " = {" + nameof(CurrentToken) + "}")]
    public class AlgebraParser
    {
        // Grammar:
        // equation : expression EQUAL expression
        // expression : term ((PLUS | MINUS) term)*
        // term : product ((STAR | SLASH) product)*
        // product : factor positive_factor*
        // factor : primary (HAT constant)?
        // primary : constant | variable | parenthesized
        // constant : MINUS? NUMBER
        // variable : X
        // parenthesized : LPAREN expression RPAREN
        // positive_factor : positive_primary (HAT constant)?
        // positive_primary : positive_constant | variable | parenthesized
        // positive_constant : NUMBER
        // Notes:
        // - Negative numbers are represented as 2 different tokens, it's our job to piece them together.
        // - To not conflict with the - operator, no-star multiplication only allows the first operand to be negative.
        //   - For example, '-2x' is allowed and parses to -2 * x. However, '2 - x' is not recognized as 2 * -x, so it becomes 2 - (minus) x.

        private readonly Func<AlgebraToken> _tokenFactory;
        private AlgebraToken _currentToken;

        public AlgebraParser(string text)
            : this(new AlgebraLexer(text))
        {
        }

        public AlgebraParser(AlgebraLexer lexer)
            : this(lexer.LexNext)
        {
        }

        public AlgebraParser(IEnumerable<AlgebraToken> tokens)
            : this(CreateFactory(tokens))
        {
        }

        public AlgebraParser(Func<AlgebraToken> tokenFactory)
        {
            _tokenFactory = tokenFactory;
            _currentToken = tokenFactory();
        }

        // For debugger use only
        private AlgebraToken CurrentToken => _currentToken;

        public ConstantNode ReadConstant() => NotNull(ReadConstantOrNull());

        public ConstantNode ReadConstantOrNull()
        {
            switch (_currentToken.Kind)
            {
                case TokenKind.Minus:
                    Eat(TokenKind.Minus);
                    return -ReadPositiveInt32();
                case TokenKind.Number:
                    return ReadPositiveInt32();
            }

            return null;
        }

        public EquationNode ReadEquation()
        {
            var lhs = ReadExpression();
            Eat(TokenKind.Equal);
            var rhs = ReadExpression();
            return Equation(lhs, rhs);
        }

        public ExpressionNode ReadExpression()
        {
            var terms = NodeList<TermNode>.CreateBuilder();
            terms.Add(ReadTerm());

            while (true)
            {
                switch (_currentToken.Kind)
                {
                    case TokenKind.Minus:
                        Eat(TokenKind.Minus);
                        terms.Add(-ReadTerm());
                        continue;
                    case TokenKind.Plus:
                        Eat(TokenKind.Plus);
                        terms.Add(ReadTerm());
                        continue;
                    default:
                        return Expression(terms.ToNodeList());
                }
            }
        }

        public FactorNode ReadFactor()
        {
            var @base = ReadPrimary();
            var exponent = ReadExponentOrNull(); // The exponent is optional, it can be null
            return Factor(@base, exponent);
        }

        public ParenthesizedNode ReadParenthesized()
        {
            Eat(TokenKind.Lparen);
            var expression = ReadExpression();
            Eat(TokenKind.Rparen);
            return Parenthesized(expression);
        }

        public ConstantNode ReadPositiveConstant() => ReadPositiveInt32();

        public FactorNode ReadPositiveFactorOrNull()
        {
            var @base = ReadPositivePrimaryOrNull();
            if (@base != null)
            {
                var exponent = ReadExponentOrNull(); // The exponent is optional, it can be null
                return Factor(@base, exponent);
            }

            return null;
        }

        public PrimaryNode ReadPositivePrimaryOrNull()
        {
            switch (_currentToken.Kind)
            {
                case TokenKind.Lparen:
                    return ReadParenthesized();
                case TokenKind.Number:
                    return ReadPositiveConstant();
                case TokenKind.X:
                    return ReadVariable();
            }

            return null;
        }

        public PrimaryNode ReadPrimary() => NotNull(ReadPrimaryOrNull());

        public PrimaryNode ReadPrimaryOrNull()
        {
            switch (_currentToken.Kind)
            {
                case TokenKind.Lparen:
                    return ReadParenthesized();
                case TokenKind.Minus:
                case TokenKind.Number:
                    return ReadConstant();
                case TokenKind.X:
                    return ReadVariable();
            }

            return null;
        }

        public NodeList<FactorNode> ReadProduct()
        {
            var factors = NodeList<FactorNode>.CreateBuilder();

            // The first factor is required; subsequent ones aren't.
            // Additionally, subsequent factors must be positive to avoid 'x - 2' being interpreted as x * -2.
            var factor = ReadFactor();
            do
            {
                factors.Add(factor);
                factor = ReadPositiveFactorOrNull();
            }
            while (factor != null);

            return factors.ToNodeList();
        }

        public TermNode ReadTerm()
        {
            var numerator = ReadProduct().ToBuilder();
            var denominator = NodeList<FactorNode>.CreateBuilder();

            while (true)
            {
                switch (_currentToken.Kind)
                {
                    case TokenKind.Slash:
                        Eat(TokenKind.Slash);
                        denominator.AddRange(ReadProduct());
                        continue;
                    case TokenKind.Star:
                        Eat(TokenKind.Star);
                        numerator.AddRange(ReadProduct());
                        continue;
                    default:
                        return Term(numerator.ToNodeList(), denominator.ToNodeList());
                }
            }
        }

        public VariableNode ReadVariable()
        {
            Eat(TokenKind.X);
            return Variable();
        }

        private AlgebraToken Eat(TokenKind kind)
        {
            var token = _currentToken;
            if (token.IsKind(kind))
            {
                _currentToken = _tokenFactory();
                return token;
            }

            RaiseTokenKindError(kind, _currentToken.Kind);
            return default(AlgebraToken);
        }

        private ConstantNode ReadExponentOrNull()
        {
            if (_currentToken.IsKind(TokenKind.Hat))
            {
                Eat(TokenKind.Hat);
                return ReadConstant();
            }

            return null;
        }

        private int ReadPositiveInt32()
        {
            var numberToken = (NumberToken)Eat(TokenKind.Number);
            return numberToken.ParseAsInt32();
        }

        private static Func<T> CreateFactory<T>(IEnumerable<T> source)
        {
            var enumerator = source.GetEnumerator();
            return () =>
            {
                Verify.State(enumerator.MoveNext(), Strings.EnumeratorMoveNextFailed);
                return enumerator.Current;
            };
        }

        private static TNode NotNull<TNode>(TNode node) where TNode : AstNode
        {
            if (node == null)
            {
                throw new ParserException($"Failed to read node of type {typeof(TNode).Name}");
            }

            return node;
        }

        private static void RaiseTokenKindError(TokenKind expectedKind, TokenKind actualKind)
        {
            throw new ParserException($"Expected token kind to be {expectedKind}, got {actualKind}");
        }
    }
}
