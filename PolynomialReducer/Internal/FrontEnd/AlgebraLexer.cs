﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;
using PolynomialReducer.Internal;
using PolynomialReducer.Internal.Tokens;
using static PolynomialReducer.Internal.Tokens.TokenFactory;

namespace PolynomialReducer.Internal.FrontEnd
{
    public class AlgebraLexer
    {
        private readonly string _text;
        private int _index;

        public AlgebraLexer(string text)
        {
            _text = text;
        }

        public ImmutableArray<AlgebraToken> LexAll()
        {
            var tokens = ImmutableArray.CreateBuilder<AlgebraToken>();

            AlgebraToken current;
            do
            {
                tokens.Add(current = LexNext());
            }
            while (!current.IsKind(TokenKind.Eof));

            return tokens.ToImmutable();
        }

        public AlgebraToken LexNext()
        {
            SkipWhitespace();

            if (IsPastEnd())
            {
                return Eof();
            }

            AlgebraToken token;
            if ((token = TryLexSingleton()) != null ||
                (token = TryLexNumber()) != null)
            {
                return token;
            }

            return Unknown();
        }

        private void SkipWhitespace()
        {
            while (!IsPastEnd() && CharUtilities.IsWhitespace(PeekChar()))
            {
                ReadChar();
            }
        }

        private bool IsPastEnd(int index = 0) => _index + index >= _text.Length;

        private char PeekChar(int index = 0) => _text[_index + index];

        private char ReadChar() => _text[_index++];

        private void Rewind(int count) => _index -= count;

        private AlgebraToken TryLexSingleton()
        {
            switch (ReadChar())
            {
                case '=':
                    return Equal();
                case '^':
                    return Hat();
                case '(':
                    return Lparen();
                case '-':
                    return Minus();
                case '+':
                    return Plus();
                case ')':
                    return Rparen();
                case '/':
                    return Slash();
                case '*':
                    return Star();
                case 'x':
                    return X();
            }

            Rewind(1);
            return null;
        }

        private NumberToken TryLexNumber()
        {
            int start = _index;

            // Note: Negative numbers are intentionally not recognized here.
            // They are represented as 2 tokens, 1 minus and 1 constant.
            // It's up to the parser to piece them together.
            if (IsPastEnd() || !CharUtilities.IsDigit(PeekChar()))
            {
                return null;
            }

            do
            {
                ReadChar();
            }
            while (!IsPastEnd() && CharUtilities.IsDigit(PeekChar()));

            string numberText = _text.Substring(start, _index - start);
            return Number(numberText);
        }
    }
}
