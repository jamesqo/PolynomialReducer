﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace PolynomialReducer.Internal
{
    internal static class ReflectionExtensions
    {
        public static bool IsEquatable(this Type type) => type.MakeEquatableType().IsAssignableFrom(type);

        public static Type MakeEquatableType(this Type type) => typeof(IEquatable<>).MakeGenericType(type);
    }
}
