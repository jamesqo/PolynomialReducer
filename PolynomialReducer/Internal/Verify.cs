﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using PolynomialReducer.Nodes;

namespace PolynomialReducer.Internal
{
    internal static class Verify
    {
        public static void Argument(bool condition, string message, string argumentName)
        {
            if (!condition)
            {
                throw new ArgumentException(message, argumentName);
            }
        }

        public static void NotEmpty(IEnumerable source, string sourceName)
            => Argument(source.GetEnumerator().MoveNext(), Strings.SourceCannotBeEmpty, sourceName);

        public static void NotNegative(int argument, string argumentName)
        {
            if (argument < 0)
            {
                throw new ArgumentOutOfRangeException(argumentName);
            }
        }

        public static void State(bool condition, string message)
        {
            if (!condition)
            {
                throw new InvalidOperationException(message);
            }
        }
    }
}
