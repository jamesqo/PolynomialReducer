﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialReducer.Internal
{
    internal static class CharUtilities
    {
        public static bool IsDigit(char ch) => ch >= '0' && ch <= '9';

        public static bool IsWhitespace(char ch)
        {
            switch (ch)
            {
                case ' ':
                case '\t':
                case '\n':
                case '\r':
                    return true;
            }

            return false;
        }

        // public static int ToInt32(char ch) => ch - '0';
    }
}
