﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialReducer.Internal
{
    internal static class Strings
    {
        public static string DegreeMustBeZeroIfCoefficientIsZero { get; } = "The degree must be zero if the coefficient is zero.";

        public static string DenominatorCannotBeZero { get; } = "The denominator cannot be zero.";

        public static string EnumeratorMoveNextFailed { get; } = "IEnumerator<T>.MoveNext failed.";

        public static string NodeListMustBeFlat { get; } = "The node list must be flat.";

        public static string NodeListMustBeShallow { get; } = "The node list must be shallow.";

        public static string NodeMustBeFlat { get; } = "The AST node must be flat.";

        public static string NodeMustBeShallow { get; } = "The AST node must be shallow.";

        public static string SourceCannotBeEmpty { get; } = "The source enumerable cannot be empty.";

        public static string TermsMustHaveDistinctDegrees { get; } = "The terms of the polynomial must have distinct degrees.";

        public static string ValueCannotBeNegative { get; } = "The value cannot be negative.";
    }
}
