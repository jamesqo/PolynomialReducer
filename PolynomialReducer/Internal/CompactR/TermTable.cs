﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PolynomialReducer.CompactR;
using static PolynomialReducer.CompactR.CompactRFactory;

namespace PolynomialReducer.Internal.CompactR
{
    [DebuggerDisplay(nameof(Count) + " = {" + nameof(Count) + "}")]
    [DebuggerTypeProxy(typeof(EnumerableDebuggerProxy<Term>))]
    internal partial class TermTable : IEnumerable<Term>
    {
        /// <summary>
        /// An empty <see cref="TermTable"/>.
        /// </summary>
        public static TermTable Empty { get; } = Create(ImmutableDictionary<int, int>.Empty, 0);

        /// <summary>
        /// The underlying table for this <see cref="TermTable"/>.
        /// The keys are the biased degrees of the terms.
        /// The values are the terms' coefficients.
        /// </summary>
        private readonly ImmutableDictionary<int, int> _terms;

        /// <summary>
        /// The difference between the degree each key represents, and the key's stored value in <see cref="_terms"/>.
        /// </summary>
        /// <remarks>
        /// This field allows ,<see cref="Polynomial.Builder"/> to multiply all terms by a power of x in constant time.
        /// Instead of updating the degrees of each key in the dictionary, we simply adjust this bias to change their meaning.
        /// For example, when multiplying the terms [9x^2, 4x] by x^3, we would add 3 to the bias, so the coefficient of degree 5
        /// would have a key of 2 in the dictionary.
        /// </remarks>
        private readonly int _degreeBias;

        private TermTable(ImmutableDictionary<int, int> terms, int degreeBias)
        {
            _terms = terms;
            _degreeBias = degreeBias;

            // There should be no entries with a coefficient of 0 but a nonzero degree.
            // This assert needs to come after initialization so Unbias() will work.
            Debug.Assert(!terms.Any(kvp => kvp.Value == 0 && Unbias(kvp.Key) != 0));
        }

        private static TermTable Create(ImmutableDictionary<int, int> terms, int degreeBias) => new TermTable(terms, degreeBias);

        public IEnumerable<int> Coefficients => _terms.Select(kvp => kvp.Value);

        public int Count => _terms.Count;

        public IEnumerable<int> Degrees => _terms.Select(kvp => Unbias(kvp.Key));

        public bool IsEmpty => _terms.IsEmpty;

        public int this[int degree] => _terms[Bias(degree)];

        public TermTable Add(Term term) => Create(_terms.Add(Bias(term.Degree), term.Coefficient), _degreeBias);

        public TermTable AddDegreeBias(int degreeBias) => WithDegreeBias(checked(_degreeBias + degreeBias));

        public IEnumerator<Term> GetEnumerator()
        {
            foreach (var kvp in _terms)
            {
                int degree = Unbias(kvp.Key), coefficient = kvp.Value;
                yield return coefficient * X(degree);
            }
        }

        public TermTable SetItem(int degree, int coefficient)
        {
            if (coefficient == 0)
            {
                Verify.Argument(degree == 0, Strings.DegreeMustBeZeroIfCoefficientIsZero, nameof(degree));
            }

            return Create(_terms.SetItem(Bias(degree), coefficient), _degreeBias);
        }

        public TermTable Remove(int degree) => Create(_terms.Remove(Bias(degree)), _degreeBias);

        public Builder ToBuilder() => Builder.Create(this);

        public bool TryGetValue(int degree, out int coefficient) => _terms.TryGetValue(Bias(degree), out coefficient);

        public TermTable WithDegreeBias(int degreeBias) => Create(_terms, degreeBias);

        /// <summary>
        /// Biases a term's degree for use as a dictionary key.
        /// </summary>
        /// <param name="degree">The unbiased degree.</param>
        /// <returns>The biased degree.</returns>
        private int Bias(int degree) => checked(degree - _degreeBias);

        /// <summary>
        /// Unbiases a dictionary key to get the degree of a term it represents.
        /// </summary>
        /// <param name="degree">The biased degree.</param>
        /// <returns>The unbiased degree.</returns>
        private int Unbias(int degree) => checked(degree + _degreeBias);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
