using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using PolynomialReducer.CompactR;

namespace PolynomialReducer.Internal.CompactR
{
    [DebuggerDisplay(nameof(Count) + " = {" + nameof(Count) + "}")]
    [DebuggerTypeProxy(typeof(EnumerableDebuggerProxy<Quotient>))]
    internal partial class QuotientTable : IEnumerable<Quotient>
    {
        public static QuotientTable Empty { get; } = Create(ImmutableDictionary<Polynomial, Polynomial>.Empty);

        /// <summary>
        /// The underlying table for this <see cref="QuotientTable"/>.
        /// The keys are the denominators of the quotients.
        /// The values are the numerators of the quotients.
        /// </summary>
        private readonly ImmutableDictionary<Polynomial, Polynomial> _quotients;

        private QuotientTable(ImmutableDictionary<Polynomial, Polynomial> quotients)
        {
            _quotients = quotients;
        }

        private static QuotientTable Create(ImmutableDictionary<Polynomial, Polynomial> quotients) => new QuotientTable(quotients);

        public int Count => _quotients.Count;

        public bool IsEmpty => _quotients.IsEmpty;

        public Polynomial this[Polynomial denominator] => _quotients[denominator];

        public QuotientTable Add(Quotient quotient) => Create(_quotients.Add(quotient.Denominator, quotient.Numerator));

        public IEnumerator<Quotient> GetEnumerator()
        {
            foreach (var kvp in _quotients)
            {
                yield return kvp.Value / kvp.Key;
            }
        }

        // TODO: Handle numerator == 0.
        public QuotientTable SetItem(Polynomial denominator, Polynomial numerator)
        {
            Verify.Argument(denominator != 0, Strings.DenominatorCannotBeZero, nameof(denominator));
            return Create(_quotients.SetItem(denominator, numerator));
        }

        public Builder ToBuilder() => Builder.Create(this);

        public bool TryGetValue(Polynomial denominator, out Polynomial numerator) => _quotients.TryGetValue(denominator, out numerator);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
