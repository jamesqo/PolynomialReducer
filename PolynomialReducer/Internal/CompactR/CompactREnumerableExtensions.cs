﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PolynomialReducer.CompactR;

namespace PolynomialReducer.Internal.CompactR
{
    internal static class CompactREnumerableExtensions
    {
        public static Term Product(this IEnumerable<Term> source)
        {
            var result = Term.One;

            foreach (var term in source)
            {
                result *= term;
            }

            return result;
        }

        public static Polynomial Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, Polynomial> selector)
        {
            var result = Polynomial.Zero;

            foreach (var item in source)
            {
                result += selector(item);
            }

            return result;
        }

        public static RationalSum Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, RationalSum> selector)
        {
            var result = RationalSum.Zero;

            foreach (var item in source)
            {
                result += selector(item);
            }

            return result;
        }
    }
}
