using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using PolynomialReducer.CompactR;

namespace PolynomialReducer.Internal.CompactR
{
    internal partial class QuotientTable
    {
        [DebuggerDisplay(nameof(Count) + " = {" + nameof(Count) + "}")]
        [DebuggerTypeProxy(typeof(EnumerableDebuggerProxy<Quotient>))]
        internal class Builder : IEnumerable<Quotient>
        {
            private readonly ImmutableDictionary<Polynomial, Polynomial>.Builder _builder;

            private Builder(ImmutableDictionary<Polynomial, Polynomial>.Builder builder)
            {
                _builder = builder;
            }

            internal static Builder Create(QuotientTable quotients) => new Builder(quotients._quotients.ToBuilder());

            public int Count => _builder.Count;

            public bool IsEmpty => Count == 0;

            public Polynomial this[Polynomial denominator]
            {
                get => _builder[denominator];
                set
                {
                    Verify.Argument(denominator != 0, Strings.DenominatorCannotBeZero, nameof(denominator));
                    _builder[denominator] = value;
                }
            }

            public void Add(Quotient quotient) => _builder.Add(quotient.Denominator, quotient.Numerator);

            public void Clear() => _builder.Clear();

            public bool ContainsKey(Polynomial denominator) => _builder.ContainsKey(denominator);

            public IEnumerator<Quotient> GetEnumerator()
            {
                foreach (var kvp in _builder)
                {
                    yield return kvp.Value / kvp.Key;
                }
            }

            public bool Remove(Polynomial denominator)
            {
                Verify.Argument(denominator != 0, Strings.DenominatorCannotBeZero, nameof(denominator));
                return _builder.Remove(denominator);
            }

            public QuotientTable ToTable() => QuotientTable.Create(_builder.ToImmutable());

            public bool TryGetValue(Polynomial denominator, out Polynomial numerator) => _builder.TryGetValue(denominator, out numerator);

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
    }
}
