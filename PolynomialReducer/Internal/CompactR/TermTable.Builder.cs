﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PolynomialReducer.CompactR;
using static PolynomialReducer.CompactR.CompactRFactory;

namespace PolynomialReducer.Internal.CompactR
{
    internal partial class TermTable
    {
        [DebuggerDisplay(nameof(Count) + " = {" + nameof(Count) + "}")]
        [DebuggerTypeProxy(typeof(EnumerableDebuggerProxy<Term>))]
        internal class Builder : IEnumerable<Term>
        {
            private readonly ImmutableDictionary<int, int>.Builder _builder;

            private int _degreeBias;

            private Builder(ImmutableDictionary<int, int>.Builder builder, int degreeBias)
            {
                _builder = builder;
                _degreeBias = degreeBias;
            }

            internal static Builder Create(TermTable terms) => new Builder(terms._terms.ToBuilder(), terms._degreeBias);

            public int Count => _builder.Count;

            internal bool IsEmpty => Count == 0;

            public IEnumerable<int> Degrees => _builder.Select(kvp => Unbias(kvp.Key));

            public int this[int degree]
            {
                get => _builder[Bias(degree)];
                set
                {
                    if (value == 0)
                    {
                        Verify.Argument(degree == 0, Strings.DegreeMustBeZeroIfCoefficientIsZero, nameof(degree));
                    }

                    _builder[Bias(degree)] = value;
                }
            }

            public void Add(Term term) => _builder.Add(Bias(term.Degree), term.Coefficient);

            public void AddDegreeBias(int degreeBias) => _degreeBias = checked(_degreeBias + degreeBias);

            public void Clear()
            {
                _builder.Clear();
                _degreeBias = 0;
            }

            public bool ContainsKey(int degree) => _builder.ContainsKey(Bias(degree));

            public IEnumerator<Term> GetEnumerator()
            {
                foreach (var kvp in _builder)
                {
                    int degree = Unbias(kvp.Key), coefficient = kvp.Value;
                    yield return coefficient * X(degree);
                }
            }

            public int GetValueOrDefault(int degree, int defaultValue = 0) => _builder.GetValueOrDefault(Bias(degree), defaultValue);

            public bool Remove(int degree) => _builder.Remove(Bias(degree));

            public TermTable ToTable() => TermTable.Create(_builder.ToImmutable(), _degreeBias);

            public bool TryGetValue(int degree, out int coefficient) => _builder.TryGetValue(Bias(degree), out coefficient);

            private int Bias(int degree) => checked(degree - _degreeBias);

            private int Unbias(int degree) => checked(degree + _degreeBias);

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
    }
}
