﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PolynomialReducer.Internal.CompactR
{
    /// <summary>
    /// An immutable stack of exponents used for implementing multinomial expansion.
    /// </summary>
    [DebuggerDisplay(nameof(Count) + " = {" + nameof(Count) + "}")]
    [DebuggerTypeProxy(typeof(EnumerableDebuggerProxy<int>))]
    internal struct ExponentStack : IEnumerable<int>
    {
        public static ExponentStack Empty { get; } = new ExponentStack(ImmutableStack<int>.Empty, 0, 0);

        private readonly ImmutableStack<int> _stack;

        private ExponentStack(ImmutableStack<int> stack, int count, int sum)
        {
            _stack = stack;
            Count = count;
            Sum = sum;
        }

        public int Count { get; }

        public int Sum { get; }

        public ImmutableStack<int> AsImmutableStack() => _stack;

        public IEnumerator<int> GetEnumerator() => _stack.AsEnumerable().GetEnumerator();

        public ExponentStack Push(int exponent)
        {
            var stack = _stack.Push(exponent);
            int count = checked(Count + 1);
            int sum = checked(Sum + exponent);
            return new ExponentStack(stack, count, sum);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
