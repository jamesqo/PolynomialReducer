﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace PolynomialReducer.Internal
{
    // WARNING: This is a mutable struct. Use with care, don't mark fields of this type readonly.
    internal struct Memoized<T>
    {
        private T _value;

        public bool HasValue { get; private set; }

        public T Value
        {
            get
            {
                Debug.Assert(HasValue);
                return _value;
            }
        }

        public T GetOrCompute<TState>(TState state, Func<TState, T> factory)
        {
            if (!HasValue)
            {
                _value = factory(state);
                HasValue = true;
            }

            return _value;
        }
    }
}
