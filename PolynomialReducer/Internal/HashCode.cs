﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialReducer.Internal
{
    internal struct HashCode
    {
        private readonly int _value;

        public static HashCode Empty => default(HashCode);

        private HashCode(int value) => _value = value;

        public static implicit operator int(HashCode hashCode) => hashCode._value;

        public HashCode Combine(int hashCode)
        {
            uint rol5 = ((uint)_value << 5) | ((uint)_value >> 27);
            return new HashCode(((int)rol5 + _value) ^ hashCode);
        }

        public HashCode Combine<T>(T value) => Combine(value?.GetHashCode() ?? 0);
    }
}
