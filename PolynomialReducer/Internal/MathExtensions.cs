﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PolynomialReducer.Internal.CompactR;

namespace PolynomialReducer.Internal
{
    internal static class MathExtensions
    {
        /// <summary>
        /// Computes a binomial coefficient.
        /// </summary>
        /// <param name="n">The number of items to choose from.</param>
        /// <param name="k">The number of items that are selected.</param>
        public static int Binomial(this int n, int k)
        {
            Debug.Assert(n >= 0 && k >= 0);
            Debug.Assert(n >= k);

            if (k > n / 2)
            {
                // Reduce the number of iterations
                // Handle \dbinom n 0, \dbinom n 1, \dbinom n {n - 1}, \dbinom n n with a single branch below
                k = n - k;
            }

            Debug.Assert(k <= n / 2);

            if (k <= 1)
            {
                return k == 0 ? 1 : n;
            }

            // To compute the binomial, we use the identity
            // \dbinom n k = (n * (n - 1) * (n - 2) .. (n - k + 1)) / k!
            //             = n / k * (n - 1) / (k - 1) .. (n - k + 1) / 1
            // We start our computation from the rhs.
            // Since k! divides the product of k consecutive integers, we can divide by k immediately after each multiplication
            // w/o worrying about flooring.
            // Dividing immediately by k avoids unnecessary overflows.
            int offset = n - k;

            // Since we know k >= 2, move the first few computations outside of the loop.
            // This avoids a few expensive integer divisions.
            int result;
            if (offset % 2 == 0)
            {
                // n - k even, divide (n - k + 2) by 2
                result = (offset + 2) / 2;
                result *= (offset + 1);
            }
            else
            {
                // n - k odd, divide (n - k + 1) by 2
                result = (offset + 1) / 2;
                result *= (offset + 2);
            }

            for (int i = 3; i <= k; i++)
            {
                int f = offset + i;

                // The below is equivalent to
                // result *= f; result /= i;
                // except made more resilient to overflow.
                // See https://math.stackexchange.com/a/2256979/385100 for the details.

                int q = result / i;
                int r = result - (q * i); // Equivalent to result % i, but mods are expensive
                result = f * q + f * r / i;
            }

            return result;
        }

        /// <summary>
        /// Computes the multinomial coefficient of an m-tuple whose integers sum to n.
        /// </summary>
        /// <param name="ks">The m-tuple of integers.</param>
        public static int Multinomial(this ExponentStack ks)
        {
            Debug.Assert(ks.Count >= 3);

            int result = 1;
            int remainingSum = ks.Sum;

            for (var node = ks.AsImmutableStack(); !node.IsEmpty; node = node.Pop())
            {
                int k = node.Peek();
                result *= remainingSum.Binomial(k);
                remainingSum -= k;
            }

            Debug.Assert(remainingSum == 0);
            return result;
        }

        public static int Pow(this int value, int exponent) => checked((int)Math.Pow(value, exponent));
    }
}
