﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace PolynomialReducer.Internal.Tokens
{
    public abstract class AlgebraToken : IEquatable<AlgebraToken>
    {
        protected internal AlgebraToken(TokenKind kind)
        {
            Debug.Assert(GetType().Name == $"{kind}Token");

            Kind = kind;
        }

        public TokenKind Kind { get; }

        public abstract string Text { get; }

        public override bool Equals(object obj)
            => obj is AlgebraToken other && Equals(other);

        public bool Equals(AlgebraToken other)
            => Kind == other.Kind && Text == other.Text;

        public bool IsKind(TokenKind kind) => Kind == kind;

        public override string ToString()
            => $"{nameof(Kind)}: {Kind}, {nameof(Text)}: {Text}";
    }
}
