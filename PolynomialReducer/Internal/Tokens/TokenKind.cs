﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialReducer.Internal.Tokens
{
    public enum TokenKind : byte
    {
        Eof,
        Equal,
        Hat,
        Lparen,
        Minus,
        Number,
        Plus,
        Rparen,
        Slash,
        Star,
        Unknown,
        // If 2+ variables are supported in the future, change this to 'Letter', not 'Variable'.
        // Or, possibly, Identifier. But allowing multi-letter identifiers wouldn't jibe well with no-star multiplication.
        X
    }
}
