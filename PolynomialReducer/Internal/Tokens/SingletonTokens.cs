﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PolynomialReducer.Internal.Tokens
{
    public abstract class SingletonToken<TToken> : AlgebraToken
        where TToken : SingletonToken<TToken>
    {
        protected internal SingletonToken(string text, TokenKind kind)
            : base(kind)
        {
            Debug.Assert(text != null);
            Debug.Assert(GetType() == typeof(TToken));

            Text = text;
        }

        public sealed override string Text { get; }

        // This code is only run once, so we can be super inefficient here and use reflection.
        internal static TToken Instance { get; } = (TToken)Activator.CreateInstance(typeof(TToken), nonPublic: true);
    }

    public sealed class EofToken : SingletonToken<EofToken>
    {
        private EofToken()
            : base("<EOF>", TokenKind.Eof)
        {
        }
    }

    public sealed class EqualToken : SingletonToken<EqualToken>
    {
        private EqualToken()
            : base("=", TokenKind.Equal)
        {
        }
    }

    public sealed class HatToken : SingletonToken<HatToken>
    {
        private HatToken()
            : base("^", TokenKind.Hat)
        {
        }
    }

    public sealed class LparenToken : SingletonToken<LparenToken>
    {
        private LparenToken()
            : base("(", TokenKind.Lparen)
        {
        }
    }

    public sealed class MinusToken : SingletonToken<MinusToken>
    {
        private MinusToken()
            : base("-", TokenKind.Minus)
        {
        }
    }

    public sealed class PlusToken : SingletonToken<PlusToken>
    {
        private PlusToken()
            : base("+", TokenKind.Plus)
        {
        }
    }

    public sealed class RparenToken : SingletonToken<RparenToken>
    {
        private RparenToken()
            : base(")", TokenKind.Rparen)
        {
        }
    }

    public sealed class SlashToken : SingletonToken<SlashToken>
    {
        private SlashToken()
            : base("/", TokenKind.Slash)
        {
        }
    }

    public sealed class StarToken : SingletonToken<StarToken>
    {
        private StarToken()
            : base("*", TokenKind.Star)
        {
        }
    }

    public sealed class UnknownToken : SingletonToken<UnknownToken>
    {
        private UnknownToken()
            : base("<???>", TokenKind.Unknown)
        {
        }
    }

    public sealed class XToken : SingletonToken<XToken>
    {
        private XToken()
            : base("x", TokenKind.X)
        {
        }
    }
}
