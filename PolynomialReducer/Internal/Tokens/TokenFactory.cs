﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialReducer.Internal.Tokens
{
    public static class TokenFactory
    {
        public static EofToken Eof() => EofToken.Instance;

        public static EqualToken Equal() => EqualToken.Instance;

        public static HatToken Hat() => HatToken.Instance;

        public static LparenToken Lparen() => LparenToken.Instance;

        public static MinusToken Minus() => MinusToken.Instance;

        public static NumberToken Number(string text) => new NumberToken(text);

        public static PlusToken Plus() => PlusToken.Instance;

        public static RparenToken Rparen() => RparenToken.Instance;

        public static SlashToken Slash() => SlashToken.Instance;

        public static StarToken Star() => StarToken.Instance;

        public static UnknownToken Unknown() => UnknownToken.Instance;

        public static XToken X() => XToken.Instance;
    }
}
