﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace PolynomialReducer.Internal.Tokens
{
    public sealed class NumberToken : AlgebraToken
    {
        internal NumberToken(string text)
            : base(TokenKind.Number)
            => Text = text;

        public override string Text { get; }

        public int ParseAsInt32()
        {
            int result = int.Parse(Text);
            Debug.Assert(result >= 0, "Number tokens should contain nonnegative numbers.");
            return result;
        }
    }
}
