﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PolynomialReducer.Nodes;
using static PolynomialReducer.Nodes.NodeFactory;

namespace PolynomialReducer.Internal.Nodes
{
    public static class MakeShallowExtensions
    {
        [Obsolete("A " + nameof(ConstantNode) + " is already shallow.", error: true)]
        public static ConstantNode MakeShallow(this ConstantNode node) => throw new NotSupportedException();

        public static EquationNode MakeShallow(this EquationNode node)
            => Equation(node.LeftSide.MakeShallow(), node.RightSide.MakeShallow());

        public static ExpressionNode MakeShallow(this ExpressionNode node) => Expression(node.Terms.MakeShallow());

        public static FactorNode MakeShallow(this FactorNode node) => Factor(node.Base.MakeShallow(), node.Exponent);

        public static NodeList<FactorNode> MakeShallow(this NodeList<FactorNode> nodes) => nodes.MakeShallowCore(f => f.MakeShallow());

        public static NodeList<TermNode> MakeShallow(this NodeList<TermNode> nodes) => nodes.MakeShallowCore(t => t.MakeShallow());

        public static ParenthesizedNode MakeShallow(this ParenthesizedNode node) => Parenthesized(node.Expression.MakeShallow());

        public static PrimaryNode MakeShallow(this PrimaryNode node)
        {
            switch (node.Kind)
            {
                case NodeKind.Constant:
                case NodeKind.Variable:
                    return node;
                default:
                    Debug.Assert(node.IsKind(NodeKind.Parenthesized));
                    return ((ParenthesizedNode)node).MakeShallow();
            }
        }

        public static TermNode MakeShallow(this TermNode node)
            => node.IsShallow ? node :
            Flatten(node.Numerator.MakeShallow(), node.Denominator.MakeShallow());

        [Obsolete("A " + nameof(VariableNode) + " is already shallow.", error: true)]
        public static VariableNode MakeShallow(this VariableNode node) => throw new NotSupportedException();

        private static TermNode Flatten(NodeList<FactorNode> numerator, NodeList<FactorNode> denominator)
        {
            Debug.Assert(numerator.IsShallow);
            Debug.Assert(denominator.IsShallow);

            var flattenExcess1 = ImmutableArray.CreateBuilder<FactorNode>();
            var flattenExcess2 = ImmutableArray.CreateBuilder<FactorNode>();

            var newNumerator = FlattenProduct(numerator, flattenExcess1);
            var newDenominator = FlattenProduct(denominator, flattenExcess2);

            return Term(
                newNumerator.AddRange(flattenExcess2),
                newDenominator.AddRange(flattenExcess1));
        }

        // TODO: XML documentation (here and for FlattenSum)
        private static NodeList<FactorNode> FlattenProduct(NodeList<FactorNode> factors, ImmutableArray<FactorNode>.Builder flattenExcess)
        {
            Debug.Assert(factors.IsShallow);

            var result = NodeList<FactorNode>.CreateBuilder();

            foreach (var factor in factors)
            {
                // We're only worried about parenthesized nodes because that's where fractions could be hiding.
                // It's impossible to represent a fraction in a constant (integer) node or a variable node, for example.
                if (!(factor.Base is ParenthesizedNode parenNode))
                {
                    result.Add(factor);
                    continue;
                }

                var expression = parenNode.Expression;
                var flattenedExpression = Expression(FlattenSum(expression.Terms, flattenExcess));
                Debug.Assert(flattenedExpression.IsFlat);
                // TODO: Everything is incorrect if the exponent != 1.
                result.Add(Factor(Parenthesized(flattenedExpression), factor.Exponent));
            }

            return result.ToNodeList();
        }

        private static NodeList<TermNode> FlattenSum(NodeList<TermNode> terms, ImmutableArray<FactorNode>.Builder flattenExcess)
        {
            Debug.Assert(terms.IsShallow);

            // Copy the terms into a mutable array.
            // For each term, flatten it if necessary and multiply the other terms by the flatten excess. Add that excess
            // to the builder, so the side of the fraction opposite from `factors` can be updated appropriately.
            var array = terms.ToArray();
            for (int i = 0; i < array.Length; i++)
            {
                TermNode term = array[i];
                Debug.Assert(term.IsShallow); // Precondition, should be true if `factors` is shallow

                if (term.IsFlat)
                {
                    continue;
                }

                if (term.HasDenominator)
                {
                    // We've run into a fraction n / d, where both n and d are flat polynomials.
                    // - Multiply all terms inside the parentheses by d to eliminate the fraction.
                    //   - Do this by removing d from this fraction, then multiplying all other terms by d.
                    // - Since we have scaled up the numerator/denominator the parentheses lie in by d,
                    //   add d to the flatten excess to indicate we want to scale up the other side too.
                    Debug.Assert(term.Numerator.IsFlat && term.Denominator.IsFlat);

                    var denominator = term.Denominator;
                    array[i] = term.WithoutDenominator();
                    MultiplySiblingTerms(denominator, array, exceptAt: i);
                    flattenExcess.AddRange(denominator);
                }
                else
                {
                    // The term is strictly shallow, but has no denominator.
                    // The numerator can't be flat, otherwise the term would be flat. (It also can't be deep by similar logic, so it is strictly shallow.)
                    // The numerator must contain some factor in parentheses that is strictly shallow. If all were flat, the numerator would be flat.
                    Debug.Assert(term.IsStrictlyShallow && term.Numerator.IsStrictlyShallow);
                    Debug.Assert(term.Numerator.Any(f => f.IsKind(NodeKind.Parenthesized) && f.IsStrictlyShallow));

                    // Flatten the numerator
                    int count = flattenExcess.Count;
                    array[i] = Term(FlattenProduct(term.Numerator, flattenExcess));

                    // Multiply other terms in the parentheses by the flatten excess from the numerator
                    var appendedFactors = flattenExcess.Skip(count);
                    MultiplySiblingTerms(appendedFactors, array, exceptAt: i);
                }
            }

            return NodeList<TermNode>.Create(array);
        }

        private static NodeList<TNode> MakeShallowCore<TNode>(this NodeList<TNode> nodes, Func<TNode, TNode> makeShallow)
            where TNode : AstNode
        {
            return NodeList<TNode>.CreateRange(nodes.Select(makeShallow));
        }

        private static void MultiplySiblingTerms(IEnumerable<FactorNode> factors, TermNode[] terms, int exceptAt)
        {
            for (int i = 0; i < terms.Length; i++)
            {
                if (i == exceptAt)
                {
                    continue;
                }

                terms[i] *= factors;
            }
        }
    }
}
