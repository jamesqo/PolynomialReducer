﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PolynomialReducer.Internal;
using PolynomialReducer.Internal.CompactR;

namespace PolynomialReducer.CompactR
{
    public partial class Polynomial
    {
        [DebuggerDisplay("{" + nameof(ToPolynomial) + "()}")]
        internal class Builder
        {
            /// <summary>
            /// The underlying builder for this <see cref="Builder"/>.
            /// </summary>
            private readonly TermTable.Builder _builder;

            /// <summary>
            /// The greatest degree of a term in this polynomial.
            /// </summary>
            private int _degree;

            private Builder(TermTable.Builder builder, int degree)
            {
                Debug.Assert(!builder.IsEmpty);

                _builder = builder;
                _degree = degree;
            }

            internal static Builder Create() => Create(Zero);

            internal static Builder Create(Polynomial poly) => new Builder(poly._terms.ToBuilder(), poly._degree);

            public int Degree => _degree;

            public bool IsTerm => TermCount == 1;

            private bool IsZero => IsTerm && _builder.Single() == 0;

            public int TermCount => _builder.Count;

            public IEnumerable<int> TermDegrees => _builder.Degrees;

            public int this[int degree]
            {
                get => _builder[degree];
                set
                {
                    if (value != 0)
                    {
                        // If the added term has a higher degree than us, update our degree.
                        // It's important this be called first since subsequent operations may change the value of IsZero.
                        MaybeReplaceDegree(degree);

                        MakeEmptyIfZero();
                        _builder[degree] = value;
                    }
                    else
                    {
                        if (degree == _degree)
                        {
                            // The highest-degree term will be removed. Update our degree to that of the next-highest term.
                            _degree = DegreeWithoutHighestTerm();
                        }

                        _builder.Remove(degree);
                        MakeZeroIfEmpty();
                    }
                }
            }

            public void Add(Term term)
            {
                if (!TryGetValue(term.Degree, out int coefficient))
                {
                    MakeEmptyIfZero();
                    AddNewDegree(term);
                }
                else
                {
                    this[term.Degree] = checked(coefficient + term.Coefficient);
                }
            }

            public void Add(Polynomial poly)
            {
                foreach (var term in poly)
                {
                    Add(term);
                }
            }

            // TODO: Maybe make internal if the type becomes public.
            public void AddNewDegree(Term term)
            {
                Debug.Assert(!_builder.ContainsKey(term.Degree));

                _builder.Add(term);
                MaybeReplaceDegree(term.Degree);
            }

            public void Clear()
            {
                _builder.Clear();
                _builder.Add(0);
                _degree = 0;
            }

            public int GetValueOrDefault(int degree, int defaultValue = 0)
                => _builder.GetValueOrDefault(degree, defaultValue);

            public void Multiply(Polynomial poly)
            {
                var @this = ToPolynomial();
                Clear();

                foreach (var term in poly)
                {
                    Add(@this * term);
                }
            }

            public void Multiply(Term term)
            {
                _builder.AddDegreeBias(term.Degree);
                if (term.Coefficient != 1)
                {
                    foreach (var degree in TermDegrees)
                    {
                        _builder[degree] = checked(_builder[degree] * term.Coefficient);
                    }
                }
            }

            public Polynomial ToPolynomial() => Polynomial.Create(_builder.ToTable(), _degree);

            public bool TryGetValue(int degree, out int coefficient)
                => _builder.TryGetValue(degree, out coefficient);

            private int DegreeWithoutHighestTerm() => IsTerm ? 0 : TermDegrees.Where(d => d != Degree).Max();

            /// <summary>
            /// Replaces this builder's degree with a new one if it is higher than the current degree,
            /// or if this builder is zero.
            /// </summary>
            /// <param name="degree">The degree to conditionally replace <see cref="Degree"/> with.</param>
            /// <remarks>
            /// If this builder is zero and x^-1 is added, we want the degree to be -1, not 0, even though 0
            /// is constant and deg(0) = 0 > deg(x^-1) = -1.
            /// </remarks>
            private void MaybeReplaceDegree(int degree)
            {
                if (degree > _degree || IsZero)
                {
                    _degree = degree;
                }
            }

            private void MakeEmptyIfZero()
            {
                if (IsZero)
                {
                    _builder.Clear();
                }
            }

            private void MakeZeroIfEmpty()
            {
                if (_builder.IsEmpty)
                {
                    _builder.Add(0);
                }
            }
        }
    }
}
