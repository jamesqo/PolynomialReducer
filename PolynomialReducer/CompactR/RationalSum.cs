﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PolynomialReducer.Internal;
using PolynomialReducer.Internal.CompactR;
using static PolynomialReducer.CompactR.CompactRFactory;

namespace PolynomialReducer.CompactR
{
    public partial class RationalSum : IEnumerable<Quotient>, IEquatable<RationalSum>
    {
        /// <summary>
        /// A rational sum representing the constant value -1.
        /// </summary>
        public static RationalSum Zero { get; } = 0;

        /// <summary>
        /// A rational sum representing the constant value 0.
        /// </summary>
        public static RationalSum One { get; } = 1;

        private readonly QuotientTable _quotients;

        private RationalSum(QuotientTable quotients)
        {
            _quotients = quotients;
        }

        private static RationalSum Create(QuotientTable quotients) => new RationalSum(quotients);

        internal static Builder CreateBuilder() => Builder.Create();

        public static RationalSum operator +(RationalSum left, Quotient right)
        {
            // TODO: Adding Quotient.Zero everywhere is really ugly. Consider making things just work.
            // TODO: A big pitfall: left was supposed to be compared to RationalSum.Zero, not Quotient.Zero.
            if (left == 0 || right == 0)
            {
                return left == 0 ? right : left;
            }

            if (left.TryGetValue(right.Denominator, out var numerator))
            {
                return left.SetItem(right.Denominator, numerator + right.Numerator);
            }
            
            return left.AddNewDenominator(right);
        }

        public static RationalSum operator +(Quotient left, RationalSum right) => right + left;

        public static RationalSum operator +(RationalSum left, RationalSum right)
        {
            if (left == 0 || right == 0)
            {
                return left == 0 ? right : left;
            }

            if (right.TermCount > left.TermCount)
            {
                return right + left;
            }

            Debug.Assert(left.TermCount >= right.TermCount);

            var result = left.ToBuilder();

            foreach (var quotient in right)
            {
                result.Add(quotient);
            }

            return result.ToRationalSum();
        }

        public static bool operator ==(RationalSum left, RationalSum right)
            => ReferenceEquals(left, null)
            ? ReferenceEquals(right, null)
            : left.Equals(right);

        public static bool operator ==(RationalSum left, Term right) => left.MatchTerm(out var term) && term == right;

        public static bool operator ==(Term left, RationalSum right) => right == left;

        public static implicit operator RationalSum(int value) => (Term)value;

        public static implicit operator RationalSum(Term value) => (Polynomial)value;

        public static implicit operator RationalSum(Polynomial value) => (Quotient)value;

        public static implicit operator RationalSum(Quotient value) => Create(QuotientTable.Empty.Add(value));

        public static bool operator !=(RationalSum left, RationalSum right) => !(left == right);

        public static bool operator !=(RationalSum left, Term right) => !(left == right);

        public static bool operator !=(Term left, RationalSum right) => !(left == right);

        public static RationalSum operator *(RationalSum left, Quotient right)
        {
            if (right == 0 || right == 1)
            {
                return right == 0 ? Zero : left;
            }

            var builder = CreateBuilder();

            foreach (var quotient in left)
            {
                builder.Add(quotient * right);
            }

            return builder.ToRationalSum();
        }

        public static RationalSum operator *(Quotient left, RationalSum right) => right * left;

        public static RationalSum operator *(RationalSum left, RationalSum right) => right.Sum(q => left * q);

        public static RationalSum operator -(RationalSum sum) => sum * -1;

        public static RationalSum operator -(RationalSum left, RationalSum right) => left + (-right);

        public bool IsInteger => MatchQuotient(out var quotient) && quotient.IsInteger;

        public bool IsPolynomial => MatchQuotient(out var quotient) && quotient.IsPolynomial;

        public bool IsQuotient => TermCount == 1;

        public bool IsTerm => MatchQuotient(out var quotient) && quotient.IsTerm;

        // TODO: Different name? QuotientCount? Same for the builder class.
        public int TermCount => _quotients.Count;

        public Polynomial this[Polynomial denominator] => _quotients[denominator];

        public override bool Equals(object obj)
            => obj is RationalSum other && Equals(other);

        public bool Equals(RationalSum other)
        {
            if (ReferenceEquals(other, null))
            {
                return false;
            }

            if (TermCount != other.TermCount)
            {
                return false;
            }

            // TODO: Incorrect. Order shouldn't matter.
            return this.SequenceEqual(other);
        }

        public IEnumerator<Quotient> GetEnumerator() => _quotients.GetEnumerator();

        public bool MatchInteger(out int integer)
        {
            integer = default(int);
            return MatchQuotient(out var quotient) && quotient.MatchInteger(out integer);
        }

        public bool MatchPolynomial(out Polynomial poly)
        {
            poly = default(Polynomial);
            return MatchQuotient(out var quotient) && quotient.MatchPolynomial(out poly);
        }

        public bool MatchQuotient(out Quotient quotient)
        {
            if (IsQuotient)
            {
                quotient = this.Single();
                return true;
            }

            quotient = default(Quotient);
            return false;
        }

        public bool MatchTerm(out Term term)
        {
            term = default(Term);
            return MatchQuotient(out var quotient) && quotient.MatchTerm(out term);
        }

        public RationalSum Pow(int exponent)
        {
            if (MatchQuotient(out var quotient))
            {
                return quotient.Pow(exponent);
            }

            throw new NotImplementedException();
        }

        // TODO: Handle numerator == 0.
        public RationalSum SetItem(Polynomial denominator, Polynomial numerator)
        {
            Verify.Argument(denominator != 0, Strings.DenominatorCannotBeZero, nameof(denominator));
            return Create(_quotients.SetItem(denominator, numerator));
        }

        internal Builder ToBuilder() => Builder.Create(this);

        // TODO: Sort the terms here and in Polynomial so 3x + 5 and 5 + 3x (which are considered equivalent)
        // generate the same string.
        public override string ToString()
        {
            var head = this.First();
            var tail = this.Skip(1);

            return string.Join(" ", tail.Select(q => q.ToStringForRationalSum()).Prepend(head.ToString()));
        }

        public bool TryGetValue(Polynomial denominator, out Polynomial numerator) => _quotients.TryGetValue(denominator, out numerator);

        private RationalSum AddNewDenominator(Quotient quotient)
        {
            Debug.Assert(this != Zero);
            Debug.Assert(quotient != Quotient.Zero);

            return Create(_quotients.Add(quotient));
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
