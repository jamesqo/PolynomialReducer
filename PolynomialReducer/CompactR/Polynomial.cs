﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PolynomialReducer.Internal;
using PolynomialReducer.Internal.CompactR;
using static PolynomialReducer.CompactR.CompactRFactory;

namespace PolynomialReducer.CompactR
{
    public partial class Polynomial : IEnumerable<Term>, IEquatable<Polynomial>
    {
        /// <summary>
        /// A polynomial representing the constant value -1.
        /// </summary>
        public static Polynomial NegativeOne { get; } = -1;

        /// <summary>
        /// A polynomial representing the constant value 0.
        /// </summary>
        public static Polynomial Zero { get; } = 0;

        /// <summary>
        /// A polynomial representing the constant value 1.
        /// </summary>
        public static Polynomial One { get; } = 1;

        /// <summary>
        /// The table containing this polynomial's terms.
        /// </summary>
        private readonly TermTable _terms;

        /// <summary>
        /// The greatest degree of a term in this polynomial.
        /// </summary>
        private readonly int _degree;

        private Polynomial(TermTable terms, int degree)
        {
            // There must be at least 1 term present, even for the constant 0.
            Debug.Assert(!terms.IsEmpty);

            _terms = terms;
            _degree = degree;
        }

        private static Polynomial Create(TermTable terms, int degree) => new Polynomial(terms, degree);

        internal static Builder CreateBuilder() => Builder.Create();

        public static implicit operator Polynomial(int value) => (Term)value;

        public static implicit operator Polynomial(Term value)
            => Create(
                TermTable.Empty.Add(value),
                value.Degree);

        public static Polynomial operator +(Polynomial left, Term right)
            => left.TryGetValue(right.Degree, out int coefficient)
            ? left.SetItem(right.Degree, checked(coefficient + right.Coefficient))
            : left.AddNewDegree(right);

        public static Polynomial operator +(Term left, Polynomial right) => right + left;

        public static Polynomial operator +(Polynomial left, Polynomial right)
        {
            // We want to add terms to the polynomial that already has most of them.
            // It's cheaper to add 1 new term to 1000 existing terms than to add 1000 new terms to 1.
            if (right.TermCount > left.TermCount)
            {
                return right + left;
            }

            Debug.Assert(left.TermCount >= right.TermCount);
            var builder = left.ToBuilder();

            foreach (var term in right)
            {
                int coefficient = builder.GetValueOrDefault(term.Degree);
                builder[term.Degree] = checked(coefficient + term.Coefficient);
            }

            return builder.ToPolynomial();
        }

        public static Quotient operator /(Polynomial left, Polynomial right) => Quotient.Create(left, right);

        public static bool operator ==(Polynomial left, Term right) => left.MatchTerm(out Term term) && term == right;

        public static bool operator ==(Term left, Polynomial right) => right == left;

        public static bool operator ==(Polynomial left, Polynomial right)
            => ReferenceEquals(left, null)
            ? ReferenceEquals(right, null)
            : left.Equals(right);

        public static bool operator !=(Polynomial left, Term right) => !(left == right);

        public static bool operator !=(Term left, Polynomial right) => !(left == right);

        public static bool operator !=(Polynomial left, Polynomial right) => !(left == right);

        public static Polynomial operator *(Polynomial left, Term right)
        {
            var terms = left._terms.AddDegreeBias(right.Degree);
            if (right.Coefficient != 1)
            {
                // Multiply each of the polynomial's terms by the coefficient.
                var builder = terms.ToBuilder();

                foreach (var term in terms)
                {
                    builder[term.Degree] = checked(terms[term.Degree] * right.Coefficient);
                }

                terms = builder.ToTable();
            }

            int degree = checked(left.Degree + right.Degree);
            return Create(terms, degree);
        }

        public static Polynomial operator *(Term left, Polynomial right) => right * left;

        public static Polynomial operator *(Polynomial left, Polynomial right) => right.Sum(term => left * term);

        public static Polynomial operator -(Polynomial left, Term right) => left + (-right);

        public static Polynomial operator -(Term left, Polynomial right) => left + (-right);

        public static Polynomial operator -(Polynomial left, Polynomial right) => left + (-right);

        // TODO: Rename unary operator here and other places to accept parameter 'value' instead?
        public static Polynomial operator -(Polynomial poly) => poly * -1;

        // TODO: Tests
        public IEnumerable<int> Coefficients => _terms.Coefficients;

        /// <summary>
        /// The greatest degree of a term in this polynomial.
        /// </summary>
        public int Degree => _degree;

        private bool IsBinomial => TermCount == 2;

        public bool IsConstant => MatchTerm(out Term term) && term.IsConstant;

        public bool IsTerm => TermCount == 1;

        /// <summary>
        /// The number of terms in this polynomial.
        /// </summary>
        public int TermCount => _terms.Count;

        // TODO: Tests.
        /// <summary>
        /// The degrees of all terms in this polynomial.
        /// </summary>
        public IEnumerable<int> TermDegrees => _terms.Degrees;

        public int this[int degree] => _terms[degree];

        // TODO: Override GetHashCode to throw NSE everywhere.
        public override bool Equals(object obj)
            => obj is Polynomial other && Equals(other);

        public bool Equals(Polynomial other)
        {
            if (ReferenceEquals(other, null))
            {
                return false;
            }

            // These are shortcuts we can take in the common case the other polynomial isn't equal.
            if (Degree != other.Degree || TermCount != other.TermCount)
            {
                return false;
            }

            // Two polynomials are equal if they have the same number of terms and the coefficients/degrees
            // corresponding to each term match.
            // The order in which terms are stored does not matter; 4x + 2 == 2 + 4x.

            foreach (var term in _terms)
            {
                if (!other.TryGetValue(term.Degree, out int coefficient) || coefficient != term.Coefficient)
                {
                    return false;
                }
            }

            return true;
        }

        public IEnumerator<Term> GetEnumerator() => _terms.GetEnumerator();

        public override int GetHashCode()
        {
            // We need to ensure that we combine hashes lexicographically.
            // e.g. '5 + x' should not result in a different hash than 'x + 5'.
            // TODO: Make this more efficient!
            return _terms
                // TODO: Implement IComparable<> on Term so this can be just OrderBy(term => term)
                .OrderBy(term => term.Degree)
                .Aggregate(HashCode.Empty, (acc, next) => acc.Combine(next));
        }

        public bool HasDegree(int degree) => Degree == degree;

        private bool MatchBinomial(out Term first, out Term second)
        {
            if (IsBinomial)
            {
                first = this.First();
                second = this.Last();
                return true;
            }

            first = default(Term);
            second = default(Term);
            return false;
        }

        public bool MatchConstant(out int constant)
        {
            constant = default(int);
            return MatchTerm(out Term term) && term.MatchConstant(out constant);
        }

        public bool MatchTerm(out Term term)
        {
            if (IsTerm)
            {
                term = this.Single();
                return true;
            }

            term = default(Term);
            return false;
        }

        // TODO: Make extension method, along with Square?
        // TODO: Add tests
        public Polynomial Pow(int exponent)
        {
            Verify.NotNegative(exponent, nameof(exponent));

            if (MatchTerm(out Term term))
            {
                return term.Pow(exponent);
            }

            if (MatchBinomial(out Term first, out Term second))
            {
                return BinomialPow(first, second, exponent);
            }

            return MultinomialPow(exponent);
        }

        /// <summary>
        /// Computes a binomial raised to the nth power using binomial expansion.
        /// </summary>
        /// <param name="first">The first term of the binomial.</param>
        /// <param name="second">The second term of the binomial.</param>
        /// <param name="exponent">The exponent to raise the binomial to.</param>
        private static Polynomial BinomialPow(Term first, Term second, int exponent)
        {
            void AddBinomialPair(Builder builder, int i)
            {
                int coefficient = exponent.Binomial(i);
                builder.Add(coefficient * first.Pow(i) * second.Pow(exponent - i));
                builder.Add(coefficient * first.Pow(exponent - i) * second.Pow(i));
            }

            var result = CreateBuilder();

            // Take advantage of the equality \dbinom n k = \dbinom n {n - k} and add terms
            // with matching coefficients in pairs.
            for (int i = 0; i < exponent / 2; i++)
            {
                AddBinomialPair(result, i);
            }

            if (exponent % 2 == 0)
            {
                // For even exponents, the middle term cannot be paired up with another term.
                int i = exponent / 2;
                int coefficient = exponent.Binomial(i);
                result.Add(coefficient * first.Pow(i) * second.Pow(i));
            }
            else
            {
                // For odd exponents, take care of the final 2 middle terms.
                AddBinomialPair(result, exponent / 2);
            }

            return result.ToPolynomial();
        }

        /// <summary>
        /// Computes a polynomial raised to the nth power using multinomial expansion.
        /// </summary>
        /// <param name="exponent">The exponent to raise this polynomial to.</param>
        // TODO: Move documentation for this pararmeter, it's moved to the lcl function
        /// <param name="prefix">
        /// Each term in the resultant polynomial has an exponent permutation that starts with this prefix.
        /// For example, if this polynomial is w + x + y + z, <paramref name="exponent"/> is 3, and
        /// <paramref name="prefix"/> is [1 0], then the result will be all terms that start with w^1x^0,
        /// namely 6w^1x^0y^1z^1 + 3w^1x^0y^2z^0 + 3w^1x^0y^0z^2.
        /// </param>
        /// <returns>
        /// The terms in this polynomial raised to <paramref name="exponent"/>, whose exponent permutations start with <paramref name="prefix"/>.
        /// </returns>
        private Polynomial MultinomialPow(int exponent)
        {
            // How the algorithm works:
            // - Let m be the number of terms in this polynomial, and n be the exponent.
            // - We loop over all possible m-tuples of integers that sum to n.
            //   This will yield all terms' exponent permutations in the resultant polynomial.
            //   - For example, consider (x + y + z)^3. This will have terms with the variables:
            //     x^3, x^2y, x^2z, xy^2, xyz, xz^2, y^3, y^2z, yz^2, z^3
            //     We would loop over the 3-tuples:
            //     (3 0 0), (2 1 0), (2 0 1), (1 2 0), (1 1 1), (1 0 2), (0 3 0), (0 2 1), (0 1 2), (0 0 3)
            //     Which correspond to the exponents of the variables.
            //   - The procedure is defined recursively: let the tuple's first element range from 0..n.
            //     For each value i, repeat the procedure for the rest of the tuple summing to n - i.
            // - The coefficient for each term is the multinomial coefficient of its exponent permutation.
            //   More info here: https://en.wikipedia.org/wiki/Multinomial_theorem
            //   - For example, xy^2 has an exponent permutation of (1 2 0). Its multinomial coefficient is
            //     3!/(1!2!0!) = 3, so the term with xy^2 will be 3xy^2.

            void PowCore(Builder result, ExponentStack prefix)
            {
                int remaining = exponent - prefix.Sum;

                if (prefix.Count == TermCount - 1)
                {
                    // The exponents of the preceding m - 1 variables have been determined in 'prefix'.
                    // In order for all exponents to sum to n, the last exponent must be n - their sum, or 'remaining'.
                    var exponents = prefix.Push(remaining);
                    int coefficient = exponents.Multinomial(); // Calculate the coefficient
                    // Raise each of our variables to their power in 'prefix'
                    Term variables = this.Zip(exponents, (t, exp) => t.Pow(exp)).Product();
                    result.Add(coefficient * variables);
                }
                else
                {
                    // We haven't decided the exponent for every variable yet, so keep going down the stack.
                    for (int i = 0; i <= remaining; i++)
                    {
                        PowCore(result, prefix.Push(i));
                    }
                }
            }

            var builder = CreateBuilder();
            PowCore(builder, prefix: ExponentStack.Empty);
            return builder.ToPolynomial();
        }

        public Polynomial SetItem(int degree, int coefficient)
        {
            // Necessary so we don't create a polynomial with an extra 0 term.
            if (this == 0)
            {
                return coefficient * X(degree);
            }

            int newDegree;
            TermTable newTerms;

            if (coefficient != 0)
            {
                newDegree = MaybeReplaceDegree(degree);
                newTerms = _terms.SetItem(degree, coefficient);
            }
            else
            {
                newDegree = degree != Degree
                    ? Degree
                    : DegreeWithoutHighestTerm();

                newTerms = _terms.Remove(degree);

                // Necessary so we don't create a polynomial with no terms.
                if (IsTerm && degree == Degree)
                {
                    return Zero;
                }
            }

            return Create(newTerms, newDegree);
        }

        // TODO: Consider adding Square()

        internal Builder ToBuilder() => Builder.Create(this);

        // TODO: ToString() should be sorted on the degrees of the terms, then the coefficients?
        public override string ToString()
        {
            Debug.Assert(this.Any());

            var head = this.First();
            var tail = this.Skip(1);

            return string.Join(" ", tail.Select(t => t.ToStringForPolynomial()).Prepend(head.ToString()));
        }

        public bool TryGetValue(int degree, out int coefficient) => _terms.TryGetValue(degree, out coefficient);

        private Polynomial AddNewDegree(Term term)
        {
            // Avoid appending a redundant zero from the term.
            if (term == 0)
            {
                return this;
            }

            // Avoid prepending a redundant zero from us.
            if (this == 0)
            {
                return term;
            }

            return Create(_terms.Add(term), Math.Max(Degree, term.Degree));
        }

        /// <summary>
        /// Gets the degree of this polynomial, ignoring the term with the highest degree.
        /// </summary>
        private int DegreeWithoutHighestTerm() => IsTerm ? 0 : TermDegrees.Where(d => d != Degree).Max();

        private int MaybeReplaceDegree(int degree)
        {
            Debug.Assert(this != 0);
            return Math.Max(degree, _degree);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
