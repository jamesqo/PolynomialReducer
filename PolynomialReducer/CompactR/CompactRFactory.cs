﻿using System.Collections.Generic;
using PolynomialReducer.Internal;

namespace PolynomialReducer.CompactR
{
    public static class CompactRFactory
    {
        public static Polynomial P(params int[] coefficients)
        {
            Verify.NotEmpty(coefficients, nameof(coefficients));

            var result = Polynomial.CreateBuilder();

            for (int i = 0; i < coefficients.Length; i++)
            {
                int coeff = coefficients[i];
                result.Add(coeff * X(i));
            }

            return result.ToPolynomial();
        }

        // TODO: Consider adding a Sum() extension method that doesn't require distinct degrees.
        // Note that that might get in the way since Polynomial implements IEnumerable<Term>.
        public static Polynomial P(IEnumerable<Term> terms)
        {
            Verify.NotEmpty(terms, nameof(terms));

            var seenDegrees = new HashSet<int>();

            bool AllowTerm(Term term) => seenDegrees.Add(term.Degree);

            var result = Polynomial.CreateBuilder();

            foreach (var term in terms)
            {
                Verify.Argument(AllowTerm(term), Strings.TermsMustHaveDistinctDegrees, nameof(terms));
                result.Add(term);
            }

            return result.ToPolynomial();
        }

        public static Quotient Q(Polynomial numerator) => numerator;

        public static Term X(int degree = 1) => new Term(1, degree);
    }
}
