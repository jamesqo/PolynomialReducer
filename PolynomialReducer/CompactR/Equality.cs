﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialReducer.CompactR
{
    public class Equality : IEquatable<Equality>
    {
        private Equality(Polynomial left, Polynomial right)
        {
            LeftSide = left;
            RightSide = right;
        }

        internal static Equality Create(Polynomial left, Polynomial right) => new Equality(left, right);

        public Polynomial LeftSide { get; }

        public Polynomial RightSide { get; }

        public override bool Equals(object obj)
            => obj is Equality other && Equals(other);

        public bool Equals(Equality other)
            => !ReferenceEquals(other, null)
            && LeftSide == other.LeftSide
            && RightSide == other.RightSide;
    }
}
