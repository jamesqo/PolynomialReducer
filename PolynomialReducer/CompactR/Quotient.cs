﻿using System;
using System.Collections.Generic;
using System.Text;
using PolynomialReducer.Internal;

namespace PolynomialReducer.CompactR
{
    public class Quotient : IEquatable<Quotient>
    {
        public static Quotient Zero { get; } = 0;

        public static Quotient One { get; } = 1;

        private Quotient(Polynomial numerator, Polynomial denominator)
        {
            Verify.Argument(denominator != 0, Strings.DenominatorCannotBeZero, nameof(denominator));

            Numerator = numerator;
            Denominator = denominator;
        }

        internal static Quotient Create(Polynomial numerator, Polynomial denominator) => new Quotient(numerator, denominator);

        // TODO: Maybe Quotient should store an IEnumerable<Polynomial> as its numerator/denominator,
        // for even more factor recognition opportunities.
        public static RationalSum operator +(Quotient left, Quotient right) => (RationalSum)left + right;

        public static Quotient operator /(Quotient left, Quotient right)
            => (left.Numerator * right.Denominator) / (left.Denominator * right.Numerator);

        public static bool operator ==(Quotient left, Quotient right)
            => ReferenceEquals(left, null) ? ReferenceEquals(right, null) : left.Equals(right);

        public static bool operator ==(Quotient left, Term right) => left.MatchTerm(out var term) && term == right;

        public static bool operator ==(Term left, Quotient right) => right == left;

        public static implicit operator Quotient(int value) => (Term)value;

        public static implicit operator Quotient(Term value) => (Polynomial)value;

        public static implicit operator Quotient(Polynomial value) => Create(value, denominator: Polynomial.One);

        public static bool operator !=(Quotient left, Quotient right) => !(left == right);

        public static bool operator !=(Quotient left, Term right) => !(left == right);

        public static bool operator !=(Term left, Quotient right) => !(left == right);

        public static Quotient operator *(Quotient left, Quotient right)
            => (left.Numerator * right.Numerator) / (left.Denominator * right.Denominator);

        public static Quotient operator -(Quotient quotient) => -quotient.Numerator / quotient.Denominator;

        public static RationalSum operator -(Quotient left, Quotient right) => (RationalSum)left - right;

        public Polynomial Denominator { get; }

        public bool IsInteger => IsPolynomial && Numerator.IsConstant;

        public bool IsPolynomial => Denominator == 1;

        public bool IsTerm => IsPolynomial && Numerator.IsTerm;

        public Polynomial Numerator { get; }

        public override bool Equals(object obj)
            => obj is Quotient other && Equals(other);

        public bool Equals(Quotient other)
        {
            if (ReferenceEquals(other, null))
            {
                return false;
            }

            return Numerator == other.Numerator
                && Denominator == other.Denominator;
        }

        public bool MatchInteger(out int integer)
        {
            integer = default(int);
            return MatchPolynomial(out var poly) && poly.MatchConstant(out integer);
        }

        public bool MatchTerm(out Term term)
        {
            term = default(Term);
            return MatchPolynomial(out var poly) && poly.MatchTerm(out term);
        }

        public bool MatchPolynomial(out Polynomial poly)
        {
            poly = Numerator;
            return IsPolynomial;
        }

        public Quotient Pow(int exponent) => Numerator.Pow(exponent) / Denominator.Pow(exponent);

        public override string ToString() => AppendDenominator(ParenthesizedText(Numerator));

        internal string ToStringForRationalSum()
        {
            if (Numerator.MatchTerm(out Term term) && term.Coefficient < 0)
            {
                return AppendDenominator($"- {-term}");
            }

            return $"+ {ToString()}";
        }

        private static string ParenthesizedText(Polynomial poly) => poly.IsTerm ? poly.ToString() : $"({poly})";

        private string AppendDenominator(string numeratorText)
        {
            if (Denominator == 1)
            {
                return numeratorText;
            }

            return $"{numeratorText} / {ParenthesizedText(Denominator)}";
        }
    }
}
