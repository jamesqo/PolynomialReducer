﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using PolynomialReducer.Internal;
using PolynomialReducer.Internal.Nodes;
using PolynomialReducer.Nodes;
using static PolynomialReducer.CompactR.CompactRFactory;

namespace PolynomialReducer.CompactR
{
    public static class ConversionToCompactRExtensions
    {
        public static Equality ToEquality(this EquationNode node)
        {
            Verify.Argument(node.IsFlat, Strings.NodeMustBeFlat, nameof(node));
            return node.LeftSide.ToPolynomial().EqualTo(node.RightSide.ToPolynomial());
        }

        public static Polynomial ToPolynomial(this ExpressionNode node)
        {
            Verify.Argument(node.IsFlat, Strings.NodeMustBeFlat, nameof(node));
            return node.Terms.ToPolynomial();
        }

        public static Polynomial ToPolynomial(this FactorNode node)
        {
            Verify.Argument(node.IsFlat, Strings.NodeMustBeFlat, nameof(node));

            switch (node.Base.Kind)
            {
                case NodeKind.Constant:
                    return node.ConstantValue();
                case NodeKind.Parenthesized:
                    return node.PolynomialValue();
                default:
                    return node.TermValue();
            }
        }

        public static Polynomial ToPolynomial(this NodeList<FactorNode> factors)
        {
            Verify.Argument(factors.IsFlat, Strings.NodeListMustBeFlat, nameof(factors));

            var result = Polynomial.One.ToBuilder();

            foreach (var factor in factors)
            {
                result.Multiply(factor.ToPolynomial());
            }

            return result.ToPolynomial();
        }

        public static Polynomial ToPolynomial(this NodeList<TermNode> terms)
        {
            Verify.Argument(terms.IsFlat, Strings.NodeListMustBeFlat, nameof(terms));

            var result = Polynomial.CreateBuilder();

            foreach (var term in terms)
            {
                result.Add(term.ToPolynomial());
            }

            return result.ToPolynomial();
        }

        public static Polynomial ToPolynomial(this TermNode node)
        {
            Verify.Argument(node.IsFlat, Strings.NodeMustBeFlat, nameof(node));

            var sign = node.IsNegative ? Polynomial.NegativeOne : Polynomial.One;
            var result = sign.ToBuilder();

            foreach (FactorNode factor in node.Numerator)
            {
                result.Multiply(factor.ToPolynomial());
            }

            return result.ToPolynomial();
        }

        public static Quotient ToQuotient(this TermNode node, bool makeShallow = false)
        {
            if (!makeShallow)
            {
                Verify.Argument(node.IsShallow, Strings.NodeMustBeShallow, nameof(node));
            }
            else
            {
                node = node.MakeShallow();
            }

            var result = node.Numerator.ToPolynomial() / node.Denominator.ToPolynomial();
            return node.IsNegative ? -result : result;
        }

        public static RationalEquality ToRationalEquality(this EquationNode node, bool makeShallow = false)
        {
            if (!makeShallow)
            {
                Verify.Argument(node.IsShallow, Strings.NodeMustBeShallow, nameof(node));
            }
            else
            {
                node = node.MakeShallow();
            }

            return node.LeftSide.ToRationalSum().EqualTo(node.RightSide.ToRationalSum());
        }

        public static RationalSum ToRationalSum(this ExpressionNode node, bool makeShallow = false)
        {
            if (!makeShallow)
            {
                Verify.Argument(node.IsShallow, Strings.NodeMustBeShallow, nameof(node));
            }
            else
            {
                node = node.MakeShallow();
            }

            return node.Terms.ToRationalSum();
        }

        public static RationalSum ToRationalSum(this FactorNode node, bool makeShallow = false)
        {
            if (!makeShallow)
            {
                Verify.Argument(node.IsShallow, Strings.NodeMustBeShallow, nameof(node));
            }
            else
            {
                node = node.MakeShallow();
            }

            switch (node.Base.Kind)
            {
                case NodeKind.Constant:
                    return node.ConstantValue();
                case NodeKind.Parenthesized:
                    return node.RationalSumValue();
                default:
                    return node.TermValue();
            }
        }

        public static RationalSum ToRationalSum(this NodeList<FactorNode> factors, bool makeShallow = false)
        {
            if (!makeShallow)
            {
                Verify.Argument(factors.IsShallow, Strings.NodeListMustBeShallow, nameof(factors));
            }
            else
            {
                factors = factors.MakeShallow();
            }

            var result = RationalSum.One.ToBuilder();

            foreach (var factor in factors)
            {
                result.Multiply(factor.ToRationalSum());
            }

            return result.ToRationalSum();
        }

        public static RationalSum ToRationalSum(this NodeList<TermNode> terms, bool makeShallow = false)
        {
            if (!makeShallow)
            {
                Verify.Argument(terms.IsShallow, Strings.NodeListMustBeShallow, nameof(terms));
            }
            else
            {
                terms = terms.MakeShallow();
            }

            // TODO: Consider combining series of flat terms into a single quotient.
            // For example, in '2x^3 + 9 + 2 / 4 + 2x + 2x^7', returning Q(2 * X(3) + 9) + Q(2) / 4 + Q(2 * X() + 2 * X(7))
            // instead of giving each term its own Q().

            var result = RationalSum.CreateBuilder();

            foreach (var term in terms)
            {
                result.Add(term.ToQuotient());
            }

            return result.ToRationalSum();
        }

        private static int ConstantValue(this FactorNode node)
        {
            Debug.Assert(node.Base.IsKind(NodeKind.Constant));

            var constantNode = (ConstantNode)node.Base;
            int baseValue = constantNode.Value;
            return node.Exponent != null ? baseValue.Pow(node.Exponent.Value) : baseValue;
        }

        private static Polynomial PolynomialValue(this FactorNode node)
        {
            Debug.Assert(node.Base.IsKind(NodeKind.Parenthesized));

            var parenNode = (ParenthesizedNode)node.Base;
            var poly = parenNode.Expression.ToPolynomial();
            return node.Exponent != null ? poly.Pow(node.Exponent.Value) : poly;
        }

        private static RationalSum RationalSumValue(this FactorNode node)
        {
            Debug.Assert(node.Base.IsKind(NodeKind.Parenthesized));

            var parenNode = (ParenthesizedNode)node.Base;
            var poly = parenNode.Expression.ToRationalSum();
            return node.Exponent != null ? poly.Pow(node.Exponent.Value) : poly;
        }

        private static Term TermValue(this FactorNode node)
        {
            Debug.Assert(node.Base.IsKind(NodeKind.Variable));
            return X(degree: node.Exponent?.Value ?? 1);
        }
    }
}
