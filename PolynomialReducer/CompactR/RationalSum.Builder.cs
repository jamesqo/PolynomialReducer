using System;
using System.Diagnostics;
using System.Linq;
using PolynomialReducer.Internal;
using PolynomialReducer.Internal.CompactR;
using static PolynomialReducer.CompactR.CompactRFactory;

namespace PolynomialReducer.CompactR
{
    public partial class RationalSum
    {
        [DebuggerDisplay("{" + nameof(ToRationalSum) + "()}")]
        internal class Builder
        {
            private readonly QuotientTable.Builder _builder;

            private Builder(QuotientTable.Builder builder)
            {
                Debug.Assert(!builder.IsEmpty);

                _builder = builder;
            }

            internal static Builder Create() => Create(Zero);

            internal static Builder Create(RationalSum sum) => new Builder(sum._quotients.ToBuilder());

            private bool IsZero => TermCount == 1 && _builder.Single() == 0;

            public int TermCount => _builder.Count;

            public Polynomial this[Polynomial denominator]
            {
                get => _builder[denominator];
                set
                {
                    Verify.Argument(denominator != 0, Strings.DenominatorCannotBeZero, nameof(denominator));

                    _builder[denominator] = value;

                    if (value != 0)
                    {
                        MakeEmptyIfZero();
                        _builder[denominator] = value;
                    }
                    else
                    {
                        _builder.Remove(denominator);
                        MakeZeroIfEmpty();
                    }
                }
            }

            public void Add(Quotient quotient)
            {
                if (!TryGetValue(quotient.Denominator, out var numerator))
                {
                    MakeEmptyIfZero();
                    AddNewDenominator(quotient);
                }
                else
                {
                    this[quotient.Denominator] = numerator + quotient.Numerator;
                }
            }

            public void Add(RationalSum sum)
            {
                foreach (var quotient in sum)
                {
                    Add(quotient);
                }
            }

            public void Clear()
            {
                _builder.Clear();
                _builder.Add(Quotient.Zero);
            }

            public bool ContainsKey(Polynomial denominator) => _builder.ContainsKey(denominator);

            public void Multiply(RationalSum sum)
            {
                var @this = ToRationalSum();
                Clear();

                foreach (var quotient in sum)
                {
                    Add(@this * quotient);
                }
            }

            public RationalSum ToRationalSum() => RationalSum.Create(_builder.ToTable());

            public bool TryGetValue(Polynomial denominator, out Polynomial numerator) => _builder.TryGetValue(denominator, out numerator);

            private void AddNewDenominator(Quotient quotient)
            {
                Debug.Assert(!ContainsKey(quotient.Denominator));

                _builder.Add(quotient);
            }

            private void MakeEmptyIfZero()
            {
                if (IsZero)
                {
                    _builder.Clear();
                }
            }

            private void MakeZeroIfEmpty()
            {
                if (_builder.IsEmpty)
                {
                    _builder.Add(Quotient.Zero);
                }
            }
        }
    }
}
