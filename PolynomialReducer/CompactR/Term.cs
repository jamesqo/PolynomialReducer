﻿using System;
using System.Collections.Generic;
using System.Text;
using PolynomialReducer.Internal;

namespace PolynomialReducer.CompactR
{
    // TODO: Use long for the coefficient.
    public struct Term : IEquatable<Term>
    {
        public static Term Zero => default(Term);

        public static Term One => 1;

        internal Term(int coefficient, int degree)
        {
            Coefficient = coefficient;
            Degree = coefficient != 0 ? degree : 0;
        }

        public static implicit operator Term(int value)
            => new Term(coefficient: value, degree: 0);

        public static Polynomial operator +(Term left, Term right) => (Polynomial)left + right;

        // TODO: Consider subtracting powers of x from num. and denom. so we multiply by less when flattening the equation.
        // e.g. 5x^3 / 10x^2 -> 5x / 10, instead of maintaining its original form.
        // Note that this could create problems if x = 0, as what was undefined suddenly becomes defined.
        public static Quotient operator /(Term left, Term right) => (Polynomial)left / right;

        public static bool operator ==(Term left, Term right) => left.Equals(right);

        public static bool operator !=(Term left, Term right) => !(left == right);

        public static Term operator *(Term left, Term right)
            => new Term(
                coefficient: checked(left.Coefficient * right.Coefficient),
                degree: checked(left.Degree + right.Degree));

        public static Polynomial operator -(Term left, Term right) => (Polynomial)left - right;

        public static Term operator -(Term term)
            => new Term(
                coefficient: checked(-term.Coefficient),
                degree: term.Degree);

        public int Coefficient { get; }

        public int Degree { get; }

        public bool IsConstant => MatchConstant(out _);

        public override bool Equals(object obj)
            => obj is Term other && Equals(other);

        public bool Equals(Term other)
            => Coefficient == other.Coefficient
            && Degree == other.Degree;

        // TODO: Add coverage for this.
        public override int GetHashCode() =>
            HashCode.Empty.Combine(Coefficient).Combine(Degree);

        public bool HasDegree(int degree) => Degree == degree;

        // TODO: Coverage
        public bool MatchConstant(out int constant)
        {
            constant = Coefficient;
            return HasDegree(0);
        }

        public Term Pow(int exponent)
        {
            Verify.NotNegative(exponent, nameof(exponent));
            return new Term(
                coefficient: Coefficient.Pow(exponent),
                degree: checked(Degree * exponent));
        }

        // public Term Square() => this * this;

        public override string ToString()
        {
            if (!IsConstant)
            {
                switch (Coefficient)
                {
                    case -1:
                        return AppendVariable("-");
                    case 1:
                        return AppendVariable(string.Empty);
                }
            }

            return AppendVariable(Coefficient.ToString());
        }

        internal string ToStringForPolynomial()
        {
            var absoluteCoefficient =
                !IsConstant && (Coefficient == 1 || Coefficient == -1)
                ? string.Empty // Omit the '1' for + x or - x, but not for + 1 or - 1
                // TODO: Update when Term.Coefficient becomes a long
                : Math.Abs((long)Coefficient).ToString(); // Cast to long to handle int.MinValue
            string sign = Coefficient < 0 ? "-" : "+";
            return AppendVariable($"{sign} {absoluteCoefficient}");
        }

        private string AppendVariable(string coefficientText)
        {
            switch (Degree)
            {
                case 0:
                    return coefficientText;
                case 1:
                    return coefficientText + "x";
                default:
                    return coefficientText + "x^" + Degree;
            }
        }

        public Term WithCoefficient(int coefficient) => new Term(coefficient, Degree);

        public Term WithDegree(int degree) => new Term(Coefficient, degree);
    }
}
