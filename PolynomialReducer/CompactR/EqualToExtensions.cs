﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialReducer.CompactR
{
    public static class EqualToExtensions
    {
        // TODO: Consider adding Term.EqualTo(RationalSum), Polynomial.EqualTo(RationalSum) and doing away with Q(Polynomial).

        public static Equality EqualTo(this Term @this, Polynomial other) => ((Polynomial)@this).EqualTo(other);

        public static Equality EqualTo(this Polynomial @this, Polynomial other) => Equality.Create(@this, other);

        public static RationalEquality EqualTo(this Quotient @this, RationalSum other) => ((RationalSum)@this).EqualTo(other);

        public static RationalEquality EqualTo(this RationalSum @this, RationalSum other) => RationalEquality.Create(@this, other);
    }
}
