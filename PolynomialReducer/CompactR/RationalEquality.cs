﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialReducer.CompactR
{
    public class RationalEquality : IEquatable<RationalEquality>
    {
        private RationalEquality(RationalSum left, RationalSum right)
        {
            LeftSide = left;
            RightSide = right;
        }

        internal static RationalEquality Create(RationalSum left, RationalSum right) => new RationalEquality(left, right);

        public RationalSum LeftSide { get; }

        public RationalSum RightSide { get; }

        public override bool Equals(object obj)
            => obj is RationalEquality other && Equals(other);

        public bool Equals(RationalEquality other)
            => !ReferenceEquals(other, null)
            && LeftSide == other.LeftSide
            && RightSide == other.RightSide;

        public override string ToString() => $"{LeftSide} = {RightSide}";
    }
}
