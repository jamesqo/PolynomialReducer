﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialReducer
{
    public class ParserException : Exception
    {
        public ParserException(string message)
            : base(message)
        {
        }
    }
}
