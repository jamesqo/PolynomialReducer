﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialReducer.Nodes
{
    public class ParenthesizedNode : PrimaryNode, IEquatable<ParenthesizedNode>
    {
        public ParenthesizedNode(ExpressionNode expression)
            : base(NodeKind.Parenthesized)
            => Expression = expression;

        public ExpressionNode Expression { get; }

        public override bool IsFlat => Expression.IsFlat;

        public override bool IsShallow => Expression.IsShallow;

        public override bool Equals(object obj)
            => obj is ParenthesizedNode other && Equals(other);

        public bool Equals(ParenthesizedNode other)
            => other != null
            && Expression.Equals(other.Expression);

        public override string ToString() => $"({Expression})";
    }
}
