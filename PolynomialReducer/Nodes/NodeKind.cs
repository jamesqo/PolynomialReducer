﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialReducer.Nodes
{
    public enum NodeKind
    {
        Constant,
        Equation,
        Expression,
        Factor,
        Parenthesized,
        Term,
        Variable
    }
}
