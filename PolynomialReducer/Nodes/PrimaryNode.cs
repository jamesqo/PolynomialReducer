﻿using System;
using System.Collections.Generic;
using System.Text;
using static PolynomialReducer.Nodes.NodeFactory;

namespace PolynomialReducer.Nodes
{
    public abstract class PrimaryNode : AstNode
    {
        protected internal PrimaryNode(NodeKind kind)
            : base(kind)
        {
        }

        public static implicit operator PrimaryNode(int value) => Constant(value);

        public override bool IsFlat => true;

        public override bool IsShallow => true;
    }
}
