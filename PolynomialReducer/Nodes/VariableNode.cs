﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialReducer.Nodes
{
    public class VariableNode : PrimaryNode, IEquatable<VariableNode>
    {
        internal static VariableNode Instance { get; } = new VariableNode();

        private VariableNode()
            : base(NodeKind.Variable)
        {
        }

        public override bool Equals(object obj)
            => obj is VariableNode other && Equals(other);

        public bool Equals(VariableNode other) => other != null;

        // TODO: Change when supporting different variable names
        public override string ToString() => "x";
    }
}
