﻿using System;
using System.Collections.Generic;
using System.Text;
using static PolynomialReducer.Nodes.NodeFactory;

namespace PolynomialReducer.Nodes
{
    public class ConstantNode : PrimaryNode, IEquatable<ConstantNode>
    {
        public ConstantNode(int value)
            : base(NodeKind.Constant)
            => Value = value;

        public static implicit operator ConstantNode(int value) => Constant(value);

        public int Value { get; }

        public override bool Equals(object obj)
            => obj is ConstantNode other && Equals(other);

        public bool Equals(ConstantNode other) => Value == other?.Value;

        public override string ToString() => Value.ToString();
    }
}
