﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using PolynomialReducer.Internal;

namespace PolynomialReducer.Nodes
{
    public abstract class AstNode
    {
        protected internal AstNode(NodeKind kind)
        {
            Debug.Assert(GetType().Name == $"{kind}Node");
            Debug.Assert(GetType().IsEquatable(), $"{GetType().Name} needs to implement IEquatable.");

            Kind = kind;
        }

        public bool IsDeep => !IsShallow;

        public virtual bool IsFlat => throw new NotSupportedException();

        public virtual bool IsShallow => throw new NotSupportedException();

        public bool IsStrictlyShallow => !IsFlat && IsShallow;

        public NodeKind Kind { get; }

        public abstract override bool Equals(object obj);

        public bool IsKind(NodeKind kind) => Kind == kind;

        public abstract override string ToString();
    }
}
