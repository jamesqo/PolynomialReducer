﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolynomialReducer.Nodes
{
    public static class NodeFactory
    {
        public static ConstantNode Constant(int value) => new ConstantNode(value);

        public static NodeList<FactorNode> Denominator(params FactorNode[] denominator) => NodeList<FactorNode>.Create(denominator);

        public static EquationNode Equation(ExpressionNode left, ExpressionNode right) => new EquationNode(left, right);

        public static ExpressionNode Expression(NodeList<TermNode> terms) => new ExpressionNode(terms);

        public static ExpressionNode Expression(params TermNode[] terms) => Expression(NodeList<TermNode>.Create(terms));

        public static FactorNode Factor(PrimaryNode @base, ConstantNode exponent = null) => new FactorNode(@base, exponent);

        public static NodeList<FactorNode> Numerator(params FactorNode[] numerator) => NodeList<FactorNode>.Create(numerator);

        public static ParenthesizedNode Parenthesized(ExpressionNode expression) => new ParenthesizedNode(expression);

        public static TermNode Term(params FactorNode[] numerator) => Term(Numerator(numerator));

        public static TermNode Term(NodeList<FactorNode> numerator, bool negative = false)
        {
            return Term(numerator, denominator: NodeList<FactorNode>.Empty, negative: negative);
        }

        public static TermNode Term(NodeList<FactorNode> numerator, NodeList<FactorNode> denominator, bool negative = false)
        {
            return new TermNode(numerator, denominator, negative);
        }

        public static VariableNode Variable() => VariableNode.Instance;
    }
}
