﻿using System;
using System.Collections.Generic;
using System.Text;
using static PolynomialReducer.Nodes.NodeFactory;

namespace PolynomialReducer.Nodes
{
    public class EquationNode : AstNode, IEquatable<EquationNode>
    {
        public EquationNode(ExpressionNode left, ExpressionNode right)
            : base(NodeKind.Equation)
        {
            LeftSide = left;
            RightSide = right;
        }

        public override bool IsFlat => LeftSide.IsFlat && RightSide.IsFlat;

        public override bool IsShallow => LeftSide.IsShallow && RightSide.IsShallow;

        public ExpressionNode LeftSide { get; }

        public ExpressionNode RightSide { get; }

        public override bool Equals(object obj)
            => obj is EquationNode other && Equals(other);

        public bool Equals(EquationNode other)
            => other != null
            && LeftSide.Equals(other.LeftSide) && RightSide.Equals(other.RightSide);

        public override string ToString() => $"{LeftSide} = {RightSide}";
    }
}
