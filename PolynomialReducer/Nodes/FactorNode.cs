﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Text;
using PolynomialReducer.Internal;
using static PolynomialReducer.Nodes.NodeFactory;

namespace PolynomialReducer.Nodes
{
    public class FactorNode : AstNode, IEquatable<FactorNode>
    {
        public FactorNode(PrimaryNode @base, ConstantNode exponent)
            : base(NodeKind.Factor)
        {
            Base = @base;
            Exponent = exponent;
        }

        public static implicit operator FactorNode(int value) => Factor(value);

        public PrimaryNode Base { get; }

        public ConstantNode Exponent { get; }

        public override bool IsFlat => Base.IsFlat;

        public override bool IsShallow => Base.IsShallow;

        public override bool Equals(object obj)
            => obj is FactorNode other && Equals(other);

        public bool Equals(FactorNode other)
            => other != null
            && Base.Equals(other.Base)
            && (Exponent?.Equals(other.Exponent) ?? other.Exponent == null);

        public override string ToString()
            => Exponent != null ? $"{Base}^{Exponent}" : Base.ToString();
    }
}
