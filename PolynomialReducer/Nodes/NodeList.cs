﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PolynomialReducer.Internal;

namespace PolynomialReducer.Nodes
{
    // TODO: Consider implementing with ImmutableStack to avoid O(n^2) with repeated Adds
    [DebuggerDisplay(nameof(Count) + " = {" + nameof(Count) + "}")]
    [DebuggerTypeProxy(typeof(EnumerableDebuggerProxy<>))]
    public struct NodeList<TNode> : IEnumerable<TNode> where TNode : AstNode
    {
        // Although this is a mutable struct, only the reference to the inner builder will be
        // duplicated when this struct is copied. Mutable state will be shared between copies.
        [DebuggerDisplay(nameof(Count) + " = {" + nameof(Count) + "}")]
        [DebuggerTypeProxy(typeof(EnumerableDebuggerProxy<>))]
        public struct Builder : IEnumerable<TNode>
        {
            private readonly ImmutableArray<TNode>.Builder _builder;

            [EditorBrowsable(EditorBrowsableState.Never)]
            internal Builder(ImmutableArray<TNode>.Builder builder) => _builder = builder;

            public int Count => _builder.Count;

            public void Add(TNode node) => _builder.Add(node);

            public void AddRange(IEnumerable<TNode> nodes) => _builder.AddRange(nodes);

            public IEnumerator<TNode> GetEnumerator() => _builder.GetEnumerator();

            public NodeList<TNode> ToNodeList() => Wrap(_builder.ToImmutable());

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        public static NodeList<TNode> Empty => Wrap(ImmutableArray<TNode>.Empty);

        private readonly ImmutableArray<TNode> _nodes;
        // These are mutable struct fields; do not mark them as readonly.
        private Memoized<bool> _isFlat;
        private Memoized<bool> _isShallow;

        private NodeList(ImmutableArray<TNode> nodes)
            : this()
            => _nodes = nodes;

        public static NodeList<TNode> Create(params TNode[] nodes)
        {
            return new NodeList<TNode>(ImmutableArray.Create(nodes));
        }

        public static Builder CreateBuilder()
        {
            return new Builder(ImmutableArray.CreateBuilder<TNode>());
        }

        public static NodeList<TNode> CreateRange(IEnumerable<TNode> nodes)
        {
            return new NodeList<TNode>(ImmutableArray.CreateRange(nodes));
        }

        public int Count => _nodes.Length;

        public bool IsDeep => !IsShallow;

        // TODO: Ensure these properties are being cached properly.
        // How can we detect if the function is called twice for a given instance?
        public bool IsFlat
        {
            get
            {
                if (_isShallow.HasValue && !_isShallow.Value)
                {
                    return false;
                }

                return _isFlat.GetOrCompute(_nodes, ns => ns.All(n => n.IsFlat));
            }
        }

        public bool IsShallow
        {
            get
            {
                if (_isFlat.HasValue && _isFlat.Value)
                {
                    return true;
                }

                return _isShallow.GetOrCompute(_nodes, ns => ns.All(n => n.IsShallow));
            }
        }

        public bool IsStrictlyShallow => !IsFlat && IsShallow;

        public NodeList<TNode> Add(TNode node) => Wrap(_nodes.Add(node));

        public NodeList<TNode> AddRange(IEnumerable<TNode> nodes) => Wrap(_nodes.AddRange(nodes));

        public IEnumerator<TNode> GetEnumerator() => _nodes.AsEnumerable().GetEnumerator();

        public Builder ToBuilder() => new Builder(_nodes.ToBuilder());

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private static NodeList<TNode> Wrap(ImmutableArray<TNode> nodes)
        {
            return new NodeList<TNode>(nodes);
        }
    }
}
