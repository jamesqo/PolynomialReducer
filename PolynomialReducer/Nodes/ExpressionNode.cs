﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PolynomialReducer.Internal;
using static PolynomialReducer.Nodes.NodeFactory;

namespace PolynomialReducer.Nodes
{
    // TODO: There should be APIs for converting an arbitrary expression to compact form, and finding roots.
    public class ExpressionNode : AstNode, IEquatable<ExpressionNode>
    {
        public ExpressionNode(NodeList<TermNode> terms)
            : base(NodeKind.Expression)
        {
            Verify.NotEmpty(terms, nameof(terms));

            Terms = terms;
        }

        public static implicit operator ExpressionNode(int value) => Expression(value);

        public override bool IsFlat => Terms.IsFlat;

        public override bool IsShallow => Terms.IsShallow;

        public NodeList<TermNode> Terms { get; }

        public override bool Equals(object obj)
            => obj is ExpressionNode other && Equals(other);

        public bool Equals(ExpressionNode other)
            => other != null
            && Terms.SequenceEqual(other.Terms);

        public override string ToString()
        {
            var head = Terms.First();
            var tail = Terms.Skip(1);
            // The first term in an expression is never negative.
            // e.g. For '2x - 3', the first term 2x is positive.
            //      For '-2x - 3', the first term is positive with factors -2 and x. The second term is negative with a single factor 3.
            Debug.Assert(!head.IsNegative);

            return string.Join(" ", tail.Select(t => t.ToString()).Prepend(head.ToString(includeSign: false)));
        }
    }
}
