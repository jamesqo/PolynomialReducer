﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PolynomialReducer.Internal;
using static PolynomialReducer.Nodes.NodeFactory;

namespace PolynomialReducer.Nodes
{
    public class TermNode : AstNode, IEquatable<TermNode>
    {
        public TermNode(NodeList<FactorNode> numerator, NodeList<FactorNode> denominator, bool negative)
            : base(NodeKind.Term)
        {
            // There must be a numerator. Even in the term '1 / x', 1 still counts as the numerator.
            Verify.NotEmpty(numerator, nameof(numerator));

            Numerator = numerator;
            Denominator = denominator;
            IsNegative = negative;
        }

        public static implicit operator TermNode(int value) => Term(value);

        public static TermNode operator *(TermNode term, FactorNode factor)
        {
            return Term(term.Numerator.Add(factor), term.Denominator, term.IsNegative);
        }

        public static TermNode operator *(FactorNode factor, TermNode term) => term * factor;

        // TODO: Multiplying by an IEnumerable<> doesn't seem entirely appropriate.
        public static TermNode operator *(TermNode term, IEnumerable<FactorNode> factors)
        {
            return Term(term.Numerator.AddRange(factors), term.Denominator, term.IsNegative);
        }

        public static TermNode operator *(IEnumerable<FactorNode> factors, TermNode term) => term * factors;

        public static TermNode operator -(TermNode term) => Term(term.Numerator, term.Denominator, !term.IsNegative);

        // Since multiplication/division are commutative, we don't have to keep track of the
        // order in which things were multiplied/divided. 3 * 6 / 2 * 7 / 4 is represented as
        // 3 * 6 * 7 / 2 / 4.

        public NodeList<FactorNode> Denominator { get; }

        public override bool IsFlat => Numerator.IsFlat && !HasDenominator;

        public bool IsNegative { get; }

        public override bool IsShallow => Numerator.IsFlat && Denominator.IsFlat;

        public bool HasDenominator => Denominator.Any();

        public NodeList<FactorNode> Numerator { get; }

        public override bool Equals(object obj)
            => obj is TermNode other && Equals(other);

        public bool Equals(TermNode other)
            => other != null
            && IsNegative == other.IsNegative
            && Numerator.SequenceEqual(other.Numerator)
            && Denominator.SequenceEqual(other.Denominator);

        public override string ToString() => ToString(includeSign: true);

        public string ToString(bool includeSign)
        {
            string Prefix(string text)
            {
                if (!includeSign)
                {
                    return text;
                }

                var prefix = IsNegative ? "- " : "+ ";
                return prefix + text;
            }

            var multipliers = string.Join(" * ", Numerator);
            if (!HasDenominator)
            {
                return Prefix(multipliers);
            }

            var divisors = string.Join(" / ", Denominator);
            return Prefix($"{multipliers} / {divisors}");
        }

        public TermNode WithDenominator(NodeList<FactorNode> denominator)
        {
            return Term(Numerator, denominator, IsNegative);
        }

        // TODO: Consider adding Abs() extension method
        public TermNode WithIsNegative(bool negative)
        {
            return Term(Numerator, Denominator, negative);
        }

        public TermNode WithNumerator(NodeList<FactorNode> numerator)
        {
            return Term(numerator, Denominator, IsNegative);
        }

        public TermNode WithoutDenominator() => WithDenominator(NodeList<FactorNode>.Empty);
    }
}
