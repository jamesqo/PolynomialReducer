# PolynomialReducer

TODO: Polynomial reducer isn't entirely appropriate, our input is an arbitrary equation, not a poly.  Rename to something like "PolyConverter"?

A library for converting arbitrary algebraic equations to polynomials in standard form.

## Sample usage

```cs
var equation = EquationNode.Parse("3 * x^2 + 4 * x + 1 = 2 * x");
// The result of ToPolynomial is not an equation, but an expression.
// This expression represents the lhs of the final equation, which has been equated to 0.
// Coefficients have been subtracted as necessary to zero out the rhs.
var polynomial = equation.ToPolynomial(); // 3 * x^2 + 2 * x + 1

// Get the coefficients associated with each exponent.
Console.WriteLine(polynomial[exponent: 0]); // 1
Console.WriteLine(polynomial[exponent: 1]); // 2
Console.WriteLine(polynomial[exponent: 2]); // 3
```

## How it works

### Overview

- `EquationNode.Parse` lexes text into a series of tokens, which are parsed into an AST.
- It's tricky to convert the AST directly to a polynomial with integer coefficients. We have to deal with arbitrarily deep nested parentheses/fractions, so these must first be eliminated via a series of transformations.
  - Consider the equation `(5 * x^5 + 6) / (9 * x^2 + 3 / (3 * x + 5)) = x / (x + 3)`.
  - First, we eliminate nested fractions: `(5 * x^5 + 6) * (3 * x + 5) / (9 * x^2 * (3 * x + 5) + 3) = x / (x + 3)`
  - Then, we apply the distributive law: `(15 * x^6 + 25 * x^5 + 18 * x + 30) / (27 * x^3 + 25 * x^2 + 3) = x / (x + 3)`
  - Then, we cross-multiply to eliminate the remaining tier of fractions: `(15 * x^6 + 25 * x^5 + 18 * x + 30) * (x + 3) = 27 * x^4 + 25 * x^3 + 3 * x`
    - The distributive law is applied again during this stage, so the actual result would be `15 * x^7 + 70 * x^6 + 75 * x^5 + 18 * x^2 + 84 * x + 90 = 27 * x^4 + 25 * x^3 + 3 * x`
  - Finally, the lhs is equated to zero by subtracting all terms from the rhs, resulting in `15 * x^7 + 70 * x^6 + 75 * x^5 - 27 * x^4 - 25 * x^3 + 18 * x^2 + 81 * x + 90`

### In-depth look

### AST

There are several types of nodes in the AST:

- Equations
- Expressions
- Terms
- Factors
  - Vanilla factors
  - Parenthesized factors
- Primitives (TODO: rename to Primaries, or do so for Factor instead?)

Explanation:

- An equation consists of two expressions, its lhs and its rhs.
- Expressions are a list of terms added together.
- Terms have a numerator and a denominator, both consisting of a list of factors.
- Factors are either vanilla or parenthesized.
  - A vanilla factor is a primitive; it may be raised to some exponent.
  - A parenthesized factor wraps an expression to give it a higher precedence; it may be raised to some exponent.
- A primitive is either the variable `x` or some numeric constant, like `32`.

### Transformations

#### Removal of nested fractions

This step is quite complicated because the input may have arbitrarily deep nested fractions; our job is to remove all of them except the last tier. Let us define some new terminology to make the explanation of this process clearer:

- A term is a *fraction* if its denominator is non-empty.
- An expression is *standard* (as in standard form) if it does not contain fractions, either directly via its child terms or indirectly via child terms' parenthesized factors.
  - By similar criteria, terms and factors are standard. If a term itself is a fraction, then it is not standard.
  - An expression, term, or factor is *standardized* when it is multiplied by factors that remove all fractions from it.
    - For example, `x / 3 + 1` standardizes to `x + 3`.
    - Note that standardizing an expression often yields an expression with a different value. The excess factors by which the original expression had to be multiplied are the *balance excess* of standardization. In the above example, it's 3.
- A term is a *simple* if it is standard, or it only contains fractions (including itself) whose numerator and denominator are standard. Otherwise, it is *nested*.
  - By similar criteria, expressions and factors are simple.
  - A term, expression, or factor can be *simplified* without changing its value.
    - For example, the nested term `5 / (4 + 4 / x)` is equal to the simple term `5 * x / (4 * x + 4)`.

OK, we're ready to start! Given some crazy nested expression like

```
1 + 1 / (1 + 1 / (1 + 1 / (1 + 1 / (1 + x))))
```

our goal is to simplify the expression. This is done by simplifying each child term. Terms are simplified in a similar fashion to a postorder tree traversal: first we simplify the term's numerator and denominator (note that because they are not standard yet, the term is not simple). Then, we simplify the term by standardizing both sides. To do this, we go through each parenthesized factor in the numerator and standardize it. This generates a balance excess that we later append to the denominator; the same thing is done for the numerator.

A concrete example might help. Above, we're told to simplify `1 / (1 + 1 / (1 + 1 / (1 + 1 / (1 + x))))` in the parent expression. To do this, we first simplify `1 / (1 + 1 / (1 + 1 / (1 + x)))` in its denominator. The call stack keeps growing until we reach the innermost `1 / (1 + x)`, which is simple, so simplification is a no-op. Then, at `1 / (1 + 1 / (1 + x))`, we standardize the denominator `(1 + 1 / (1 + x))` by removing `(1 + x)` from the offending fraction and multiplying its sibling terms by that, creating `((1 + x) + 1)`. This creates a balance excess of `(1 + x)`, so we have to multiply the numerator by that as well; the result is `(1 + x) / ((1 + x) + 1)`. This is handed back to the previous caller, and the process continues back up the stack.

## License

[MIT](LICENSE)
