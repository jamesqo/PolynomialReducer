﻿using System;
using System.Collections.Generic;
using System.Text;
using PolynomialReducer.CompactR;
using PolynomialReducer.Nodes;
using Xunit;

namespace PolynomialReducer.Tests.Nodes
{
    public class EquationNodeTests
    {
        [Theory]
        [ClassData(typeof(E2EData_CanBeParsed))]
        public void RoundTrip(E2ETestCase testCase)
        {
            var equation = testCase.Equation;
            Assert.Equal(equation, Algebra.ParseEquation(equation.ToString()));
        }

        [Theory]
        [ClassData(typeof(E2EData_CanBeParsed))]
        public void ToEquality(E2ETestCase testCase)
        {
            if (testCase.Equality != null)
            {
                Assert.Equal(testCase.Equality, testCase.Equation.ToEquality());
            }
            else if (testCase.EqualityException != null)
            {
                Assert.Throws(testCase.EqualityException, () => testCase.Equation.ToEquality());
            }
        }

        [Theory]
        [ClassData(typeof(E2EData_CanBeParsed))]
        public void ToRationalEquality(E2ETestCase testCase)
        {
            if (testCase.RationalEquality != null)
            {
                Assert.Equal(testCase.RationalEquality, testCase.Equation.ToRationalEquality(true));
                Assert.Equal(testCase.RationalEquality, testCase.ShallowEquation.ToRationalEquality());
            }
            else if (testCase.RationalEqualityException != null)
            {
                Assert.Throws(testCase.RationalEqualityException, () => testCase.Equation.ToRationalEquality(true));
                Assert.Throws(testCase.RationalEqualityException, () => testCase.ShallowEquation.ToRationalEquality());
            }
        }

        [Theory]
        [ClassData(typeof(E2EData_CanBeParsed))]
        public void ToString(E2ETestCase testCase)
        {
            Assert.Equal(testCase.EquationToString, testCase.Equation.ToString());
        }
    }
}
