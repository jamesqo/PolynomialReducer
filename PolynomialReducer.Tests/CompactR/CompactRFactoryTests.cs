﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PolynomialReducer.CompactR;
using Xunit;
using Factory = PolynomialReducer.CompactR.CompactRFactory;

namespace PolynomialReducer.Tests.CompactR
{
    public class CompactRFactoryTests
    {
        [Theory]
        [MemberData(nameof(P_Coefficients_Data))]
        public void P_Coefficients(IEnumerable<int> coefficients, Polynomial expected)
        {
            Assert.Equal(expected, Factory.P(coefficients.ToArray()));
        }

        public static IEnumerable<object[]> P_Coefficients_Data()
            => new[]
            {
                new object[] { new[] { 1, 2, 3 }, 1 + 2 * Factory.X() + 3 * Factory.X(2) },
                new object[] { new[] { 0 }, Polynomial.Zero },
                new object[] { new[] { 1 }, Polynomial.One },
                new object[] { new[] { -1, -1, -2 }, -1 - Factory.X() - 2 * Factory.X(2) }
            };

        [Fact]
        public void P_Coefficients_Empty_Invalid()
        {
            Assert.Throws<ArgumentException>(() => Factory.P());
        }

        [Theory]
        [MemberData(nameof(P_Terms_Data))]
        public void P_Terms(IEnumerable<Term> terms, Polynomial expected)
        {
            Assert.Equal(expected, Factory.P(terms));
        }

        public static IEnumerable<object[]> P_Terms_Data()
            => new[]
            {
                new object[] { new[] { Factory.X(), Factory.X(2) }, Factory.X() + Factory.X(2) },
                new object[] { new[] { 3 * Factory.X(-1), -4 * Factory.X(2), 5 }, 3 * Factory.X(-1) - 4 * Factory.X(2) + 5 },
                new object[] { new[] { Term.Zero }, Polynomial.Zero },
                new object[] { new[] { Term.One }, Polynomial.One }
            };

        [Theory]
        [MemberData(nameof(P_Terms_DuplicateDegrees_Invalid_Data))]
        public void P_Terms_DuplicateDegrees_Invalid(IEnumerable<Term> terms)
        {
            Assert.NotEmpty(terms);
            Assert.Contains(terms, t1 => terms.Count(t2 => t2.HasDegree(t1.Degree)) >= 2); // Ensure there are two terms with the same degree.

            Assert.Throws<ArgumentException>(() => Factory.P(terms));
        }

        public static IEnumerable<object[]> P_Terms_DuplicateDegrees_Invalid_Data()
            => new[]
            {
                new object[] { new[] { Factory.X(), Factory.X() } },
                new object[] { new[] { 3 * Factory.X(2), 5 * Factory.X(3), 6 * Factory.X(2) } },
                new object[] { new[] { 5, Factory.X(), 6 } }
            };

        [Fact]
        public void P_Terms_Empty_Invalid()
        {
            Assert.Throws<ArgumentException>(() => Factory.P(Array.Empty<Term>()));
        }

        [Theory]
        [ClassData(typeof(Polynomial_Data))]
        public void Q(Polynomial numerator)
        {
            var q = Factory.Q(numerator);
            Assert.Equal(numerator, q);
            Assert.Equal(numerator, q.Numerator);
            Assert.Equal(1, q.Denominator);
        }

        [Theory]
        [ClassData(typeof(Int32_Data))]
        public void X(int degree)
        {
            var term = Factory.X(degree);
            Assert.Equal(1, term.Coefficient);
            Assert.Equal(degree, term.Degree);
        }

        [Fact]
        public void X_NoDegree()
        {
            var x = Factory.X();
            Assert.Equal(1, x.Coefficient);
            Assert.Equal(1, x.Degree);
        }
    }
}
