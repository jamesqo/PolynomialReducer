﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PolynomialReducer.CompactR;
using PolynomialReducer.Tests.TestInternal;
using Xunit;
using static PolynomialReducer.CompactR.CompactRFactory;

namespace PolynomialReducer.Tests.CompactR
{
    public class PolynomialTests
    {
        [Theory]
        [MemberData(nameof(Add_Subtract_Negate_Data))]
        public void Add_Subtract_Negate(Polynomial first, Polynomial second, Polynomial sum)
        {
            void Verify_Polynomial_Polynomial()
            {
                Assert.Equal(sum, first + second);
                Assert.Equal(sum, second + first);

                Assert.Equal(sum, first - (-second));
                Assert.Equal(sum, second - (-first));

                Assert.Equal(-sum, -first - second);
                Assert.Equal(-sum, -second - first);

                Assert.Equal(sum - 2 * first, second - first);
                Assert.Equal(sum - 2 * second, first - second);
            }

            void Verify_Polynomial_Term(Polynomial poly, Term term)
            {
                Assert.Equal(sum, poly + term);
                Assert.Equal(sum, term + poly);

                Assert.Equal(sum, poly - (-term));
                Assert.Equal(sum, term - (-poly));

                Assert.Equal(-sum, -poly - term);
                Assert.Equal(-sum, -term - poly);

                Assert.Equal(sum - 2 * poly, term - poly);
                Assert.Equal(sum - 2 * term, poly - term);
            }

            Verify_Polynomial_Polynomial();

            if (first.MatchTerm(out var firstAsTerm))
            {
                Verify_Polynomial_Term(second, firstAsTerm);
            }

            if (second.MatchTerm(out var secondAsTerm))
            {
                Verify_Polynomial_Term(first, secondAsTerm);
            }
        }

        public static IEnumerable<object[]> Add_Subtract_Negate_Data()
            => new[]
            {
                new object[] { (Polynomial)X(), (Polynomial)X(2), X() + X(2) }, // No like terms, inputs don't use operator+
                new object[] { (Polynomial)(3 * X()), (Polynomial)(4 * X()), (Polynomial)(7 * X()) }, // Like terms, inputs don't use operator+
                new object[] { X() + 3, X() + 4, 2 * X() + 7 }, // Like terms, inputs use operator+
                new object[] { X(3) + X(), 3 * X(2) + 1, X(3) + 3 * X(2) + X() + 1 }, // No like terms, inputs use operator+
                new object[] { -1 * X(-1) - 9, (Polynomial)(3 * X(-2)), 3 * X(-2) + -1 * X(-1) - 9 } // Negative coefficients/degrees
            };

        [Theory]
        [MemberData(nameof(Coefficients_Data))]
        public void Coefficients(Polynomial poly, IEnumerable<int> expected)
        {
            Assert.NotEmpty(expected);

            // The coefficients are not guaranteed to be ordered whatsoever, even for polynomials that are considered equal.
            Assert.Equal(expected, poly.Coefficients.OrderBy(i => i));
        }

        public static IEnumerable<object[]> Coefficients_Data()
            => new[]
            {
                new object[] { Polynomial.Zero, new[] { 0 } },
                new object[] { Polynomial.One, new[] { 1 } },
                new object[] { (Polynomial)X(), new[] { 1 } },
                new object[] { 4 * X(2) - 2 + X(-2), new[] { -2, 1, 4 } }
            };

        [Theory]
        [ClassData(typeof(Polynomial_Data))]
        public void Degree(Polynomial poly)
        {
            Assert.Equal(poly.Max(t => t.Degree), poly.Degree);
        }

        [Theory]
        [MemberData(nameof(Equals_False_Data))]
        public void Equals_False(Polynomial poly, object obj)
        {
            Assert.NotNull(poly);
            Assert.NotNull(obj);

            Assert.False(poly.Equals(obj) || obj.Equals(poly));

            if (obj is Polynomial other)
            {
                Assert.False(poly.Equals(other) || other.Equals(poly));
                Assert.False(poly == other || other == poly);
                Assert.True(poly != other && other != poly);
            }
        }

        public static IEnumerable<object[]> Equals_False_Data()
            => new[]
            {
                // Int32 and Term can be implicitly converted to, but are not, Polynomials
                new object[] { (Polynomial)3, 3 },
                new object[] { (Polynomial)X(), X() },
                new object[] { Polynomial.Zero, new object() },
                new object[] { (Polynomial)X(2), (Polynomial)X() }, // Different Degree
                new object[] { (Polynomial)X(), X() + 4 }, // Different TermCount
                new object[] { X(2) + X(), X(2) + 3 }, // Same Degree && TermCount, different TermDegrees
                new object[] { X(2) + 3, X(2) + 4 } // Same Degree && TermCount && TermDegrees, different coefficients
            };

        [Theory]
        [ClassData(typeof(Polynomial_Data))]
        public void Equals_False_Null(Polynomial poly)
        {
            Assert.NotNull(poly);

            Assert.False(poly.Equals(obj: null) || poly.Equals(other: null));
            Assert.False(poly == null || null == poly);
            Assert.True(poly != null && null != poly);
        }

        [Theory]
        [ClassData(typeof(Polynomial_Data))]
        public void Equals_True(Polynomial poly)
        {
            Assert.NotNull(poly);
            Assert.IsAssignableFrom<IEquatable<Polynomial>>(poly);

            void Verify(Polynomial other)
            {
                Assert.NotNull(other);

                Assert.True(poly.Equals(other) && other.Equals(poly));
                Assert.True(poly.Equals((object)other) && other.Equals((object)poly));

                Assert.True(poly == other && other == poly);
                Assert.False(poly != other || other != poly);

                Assert.False(poly == null || !(poly != null) || poly.Equals(null));
                Assert.False(other == null || !(other != null) || other.Equals(null));

                if (poly.MatchTerm(out var term))
                {
                    Assert.True(other.IsTerm);
                    Assert.True(other.MatchTerm(out var otherTerm));

                    // Test equality operators accepting (Polynomial, Term)

                    Assert.True(poly == otherTerm && otherTerm == poly);
                    Assert.False(poly != otherTerm || otherTerm != poly);

                    Assert.True(other == term && term == other);
                    Assert.False(other != term || term != other);
                }
            }

            Assert.All(EqualPolynomials(poly), Verify);
        }

        [Fact]
        public void Equals_True_Null_Null()
        {
            Assert.True((Polynomial)null == null);
            Assert.False((Polynomial)null != null);
        }

        [Theory]
        [ClassData(typeof(Polynomial_Data))]
        public void GetEnumerator(Polynomial poly)
        {
            Assert.NotSame(poly.GetEnumerator(), poly.GetEnumerator());
            Assert.NotSame(((IEnumerable)poly).GetEnumerator(), ((IEnumerable)poly).GetEnumerator());

            Assert.IsAssignableFrom<IEnumerator<Term>>(((IEnumerable)poly).GetEnumerator());

            var firstSnapshot = poly.Where(t => true).ToList();
            var secondSnapshot = poly.Where(t => true).ToList();
            Assert.Equal(firstSnapshot, secondSnapshot);
        }

        [Theory]
        [ClassData(typeof(Polynomial_Data))]
        public void GetItem(Polynomial poly)
        {
            foreach (int degree in poly.TermDegrees)
            {
                int expected = poly.Single(t => t.Degree == degree).Coefficient;
                Assert.Equal(expected, poly[degree]);
            }
        }

        [Theory]
        [ClassData(typeof(Polynomial_Data))]
        public void HasDegree(Polynomial poly)
        {
            Assert.True(poly.HasDegree(poly.Degree));
            Assert.False(poly.HasDegree(poly.Degree + 1));
            Assert.False(poly.HasDegree(poly.Degree - 1));
        }

        [Theory]
        [ClassData(typeof(Term_Data))]
        public void ImplicitConversion(Term term)
        {
            void Verify(Polynomial poly)
            {
                Assert.Equal(1, poly.TermCount);
                Assert.Equal(term.Degree, poly.Degree);
                Assert.Equal(term, poly.Single()); // The single Term is extracted from the Polynomial
                Assert.Equal(term, poly); // The Term is implicitly converted to a Polynomial
            }

            Verify(term);
            if (term.MatchConstant(out int constant))
            {
                Verify(constant);
            }
        }

        [Theory]
        [ClassData(typeof(Polynomial_Data))]
        public void IsConstant(Polynomial poly)
        {
            if (poly.IsConstant)
            {
                Assert.True(poly.IsTerm);

                Assert.True(poly.MatchConstant(out int constant));
                Assert.True(poly.MatchTerm(out var term));

                Assert.Equal(term, constant);
                Assert.Equal(poly, constant);
            }
            else
            {
                Assert.False(poly.MatchConstant(out _));
            }
        }

        [Theory]
        [ClassData(typeof(Polynomial_Data))]
        public void IsTerm(Polynomial poly)
        {
            if (poly.IsTerm)
            {
                Assert.True(poly.MatchTerm(out var term));
                Assert.Equal(poly, term);
            }
            else
            {
                Assert.False(poly.MatchTerm(out _));
            }
        }

        [Theory]
        [MemberData(nameof(Multiply_Data))]
        public void Multiply(Polynomial first, Polynomial second, Polynomial expected)
        {
            void Verify_Polynomial_Polynomial()
            {
                Assert.Equal(expected, first * second);
                Assert.Equal(expected, second * first);
            }

            void Verify_Polynomial_Term(Polynomial poly, Term term)
            {
                Assert.Equal(expected, poly * term);
                Assert.Equal(expected, term * poly);
            }

            Verify_Polynomial_Polynomial();

            if (first.MatchTerm(out var firstAsTerm))
            {
                Verify_Polynomial_Term(second, firstAsTerm);
            }

            if (second.MatchTerm(out var secondAsTerm))
            {
                Verify_Polynomial_Term(first, secondAsTerm);
            }
        }

        public static IEnumerable<object[]> Multiply_Data()
            => new[]
            {
                new object[] { (Polynomial)X(), (Polynomial)X(), (Polynomial)X(2) }, // Both are terms
                new object[] { X() + 3, (Polynomial)X(), X(2) + 3 * X() }, // One is a term
                new object[] { X(2) + 2, X(3) + X(), X(5) + 3 * X(3) + 2 * X() }, // Like terms
                new object[] { X() + X(-1), X() + X(-1), X(2) + 2 + X(-2) }, // Negative coefficients, like terms
                new object[] { 5 * X(2) + 3 * X(), 2 * X(3) + 1, 10 * X(5) + 6 * X(4) + 5 * X(2) + 3 * X() } // No like terms
            };

        [Fact]
        public void One()
        {
            var one = Polynomial.One;

            Assert.Same(one, Polynomial.One);
            Assert.Equal(one, Polynomial.One);

            Assert.Equal(1, one.TermCount);
            Assert.Equal(1, one[0]);
            Assert.True(one.IsConstant);
            Assert.True(one != 0);
        }

        [Theory]
        [MemberData(nameof(Pow_Data))]
        public void Pow(Polynomial poly, int exponent, Polynomial expected)
        {
            Assert.Equal(expected, poly.Pow(exponent));
        }

        public static IEnumerable<object[]> Pow_Data()
        {
            var list = new List<object[]>();
            list.Add(new object[] { (Polynomial)(5 * X(2)), 4, (Polynomial)(625 * X(8)) }); // Monomial
            // Binomial, odd exponent
            list.Add(new object[]
            {
                X() + X(-1),
                5,
                X(5) + 5.Choose(1) * X(3) + 5.Choose(2) * X(1) + 5.Choose(3) * X(-1) + 5.Choose(4) * X(-3) + X(-5)
            });
            // Binomial, even exponent
            list.Add(new object[]
            {
                2 * X() - 1,
                4,
                16 * X(4) - 4.Choose(1) * 8 * X(3) + 4.Choose(2) * 4 * X(2) - 4.Choose(3) * 2 * X() + 1
            });
            // Multinomial (3+ terms in base)
            var a = 5 * X(2);
            var b = 9 * X(-2);
            var c = 2;
            list.Add(new object[]
            {
                a + b + c,
                3,
                a.Pow(3) + 3.Choose(2, 1) * a.Pow(2) * b + 3.Choose(2, 1) * a.Pow(2) * c + 3.Choose(1, 2) * a * b.Pow(2) + 3.Choose(1, 1, 1) * a * b * c + 3.Choose(1, 2) * a * c.Pow(2) + b.Pow(3) + 3.Choose(2, 1) * b.Pow(2) * c + 3.Choose(1, 2) * b * c.Pow(2) + c.Pow(3)
            });

            return list;
        }

        [Theory]
        [ClassData(typeof(Pow_Negative_Invalid_Data))]
        public void Pow_Negative_Invalid_Data(int exponent)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => ((Polynomial)X()).Pow(exponent));
        }

        [Theory]
        [MemberData(nameof(SetItem_Data))]
        public void SetItem(Polynomial poly, int degree, int coefficient, Polynomial expected)
        {
            Assert.Equal(expected, poly.SetItem(degree, coefficient));
        }

        public static IEnumerable<object[]> SetItem_Data()
            => new[]
            {
                new object[] { X(3) + 2 * X(1), 1, 5, X(3) + 5 * X(1) }, // Replace existing coefficient with nonzero one (Degree not updated)
                new object[] { X(3) + 2 * X(1), 3, 0, (Polynomial)(2 * X(1)) }, // Replace highest coefficient with zero (Degree updated)
                new object[] { X(3) + 2, -1, 6, X(3) + 2 + 6 * X(-1) }, // Add new coefficient (Degree not updated)
                new object[] { X(3) + 2, 5, 6, 6 * X(5) + X(3) + 2 }, // Add higher-degree term (Degree updated)
                new object[] { X(2) + X(), 2, 0, (Polynomial)X() }, // Zero out term, remainder non-zero (Degree updated)
                new object[] { (Polynomial)X(2), 2, 0, Polynomial.Zero }, // Zero out term, remainder zero (Degree updated)
                new object[] { (Polynomial)X(-2), -2, 0, Polynomial.Zero }, // Zero out negative-degree term, remainder zero (Degree updated)
                new object[] { Polynomial.Zero, -1, 1, (Polynomial)X(-1) }, // Source is constant, zero: Degree should be negative if a negative-degree term is set
                new object[] { Polynomial.One, -1, 1, 1 + X(-1) }, // Source is constant, nonzero: Degree should stay put if a negative-degree term is set
                new object[] { X() + 1, 2, 0, X() + 1 }, // 0 replaced with 0 has no effect
                new object[] { Polynomial.Zero, 1, 0, Polynomial.Zero }, // Source is 0, degree > 0, coefficient is 0
                new object[] { Polynomial.Zero, -1, 0, Polynomial.Zero }, // Source is 0, degree < 0, coefficient is 0
                new object[] { Polynomial.Zero, 0, 0, Polynomial.Zero } // Source is 0, degree is 0, coefficient is 0
            };

        [Theory]
        [ClassData(typeof(Polynomial_Data))]
        public void TermCount(Polynomial poly)
        {
            // Where().Count() can't take any shortcuts and actually iterates the sequence to get the count.
            int expectedCount = poly.Where(t => true).Count();
            Assert.Equal(expectedCount, poly.TermCount);
        }

        [Theory]
        [MemberData(nameof(ToString_Data))]
        public void ToString(Polynomial poly, string expected)
        {
            Assert.Equal(expected, poly.ToString());
        }

        public static IEnumerable<object[]> ToString_Data()
            => new[]
            {
                new object[] { (Polynomial)X(), "x" }, // Coefficient of 1 omitted
                // ToString should return the same text regardless of how the polynomial is constructed.
                new object[] { 1 + X() + X(2), "1 + x + x^2" },
                new object[] { X(2) + X() + 1, "1 + x + x^2" },
                new object[] { Polynomial.Zero, "0" }, // 0 (only polynomial that contains a 0 coefficient)
                new object[] { Polynomial.One, "1" }, // 1 (omitted as a coefficient in non-constant terms)
                new object[] { (Polynomial)(-1), "-1" }, // -1 (only the - sign is shown for non-constant terms)
                new object[] { -X(-2) + 1, "-x^-2 + 1" }, // Coefficient of -1: only - sign shown (first term), negative degree
                new object[] { -2 * X(-2) + 1, "-2x^-2 + 1" }, // Non-(-1) negative coefficient (first term), negative degree
                new object[] { 1 - X(2), "1 - x^2" }, // Coefficient of -1: only - sign shown (not first term)
                new object[] { 1 - 2 * X(2), "1 - 2x^2" } // Non-(-1) negative coefficient (not first term)
            };

        [Theory]
        [MemberData(nameof(TryGetValue_Data))]
        public void TryGetValue(Polynomial poly, int degree, int? expected)
        {
            Assert.Equal(expected.HasValue, poly.TryGetValue(degree, out int coefficient));
            Assert.Equal(expected.GetValueOrDefault(), coefficient);
        }

        public static IEnumerable<object[]> TryGetValue_Data()
            => new[]
            {
                new object[] { (Polynomial)X(4), 4, 1 }, // Single-term polynomial, present
                new object[] { (Polynomial)X(4), 3, null }, // Single-term polynomial, not present
                new object[] { X(2) + 3 * X(-1), -1, 3 }, // Multiple-term polynomial, present
                new object[] { X(2) + 3 * X(-1), 0, null }, // Multiple-term polynomial, not present
                new object[] { (Polynomial)(0 * X(4)), 4, null }, // Zero-valued polynomial, not present
                new object[] { Polynomial.Zero, 0, 0 }, // Zero-valued polynomial, present
                new object[] { Polynomial.One, 0, 1 }, // Zero-degree polynomial, present
                new object[] { (Polynomial)2, -1, null } // Zero-degree polynomial, not present
            };

        [Fact]
        public void Zero()
        {
            var zero = Polynomial.Zero;

            Assert.Same(zero, Polynomial.Zero);
            Assert.Equal(zero, Polynomial.Zero);

            Assert.Equal(1, zero.TermCount);
            Assert.Equal(0, zero[0]);
            Assert.True(zero.IsConstant);
            Assert.True(zero == 0);
        }

        private static IEnumerable<Polynomial> EqualPolynomials(Polynomial poly)
        {
            yield return poly;
            yield return poly.Aggregate(Polynomial.Zero, (acc, t) => acc + t);
        }
    }
}
