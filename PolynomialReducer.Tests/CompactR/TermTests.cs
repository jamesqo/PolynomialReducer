﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PolynomialReducer.CompactR;
using Xunit;
using static PolynomialReducer.CompactR.CompactRFactory;

namespace PolynomialReducer.Tests.CompactR
{
    public class TermTests
    {
        [Theory]
        [ClassData(typeof(Term_Term_Data))]
        public void Add_Term_Term(Term first, Term second)
        {
            void Verify(Polynomial sum)
            {
                Assert.Equal((Polynomial)first + second, sum);
                Assert.Equal(first + (Polynomial)second, sum);
                Assert.Equal((Polynomial)first + (Polynomial)second, sum);
            }

            Verify(first + second);
            Verify(second + first);
        }

        [Theory]
        [MemberData(nameof(Equals_False_Data))]
        public void Equals_False(Term term, object obj)
        {
            Assert.False(term.Equals(obj) || obj?.Equals(term) == true);

            if (obj is Term other)
            {
                Assert.False(term.Equals(other) || other.Equals(term));
                Assert.False(term == other || other == term);
                Assert.True(term != other && other != term);
            }
        }

        public static IEnumerable<object[]> Equals_False_Data()
            => new[]
            {
                // While there is an implicit conversion from Int32 -> Term, that doesn't mean an Int32 and Term
                // should be considered equal because an Int32 is not a Term.
                new object[] { (Term)3, 3 },
                new object[] { X(3), X(4) },
                new object[] { 3 * X(2), 2 * X(3) },
                new object[] { X(), null }
            };

        [Theory]
        [ClassData(typeof(Term_Data))]
        public void Equals_True(Term term)
        {
            Assert.IsAssignableFrom<IEquatable<Term>>(term);

            var equivalent = term.Coefficient * X(term.Degree);

            Assert.True(term.Equals(equivalent) && equivalent.Equals(term));
            Assert.True(term.Equals((object)equivalent) && equivalent.Equals((object)term));

            Assert.True(term == equivalent && equivalent == term);
            Assert.False(term != equivalent || equivalent != term);
        }

        [Theory]
        [ClassData(typeof(Int32_Data))]
        public void ImplicitConversion_Int32(int constant)
        {
            Term term = constant;

            Assert.Equal(constant, term.Coefficient);
            Assert.True(term.IsConstant);
            Assert.True(term.MatchConstant(out int actualConstant));
            Assert.Equal(constant, actualConstant);
        }

        [Theory]
        [ClassData(typeof(Term_Data))]
        public void IsConstant(Term term)
        {
            Assert.Equal(term.HasDegree(0), term.IsConstant);
        }

        [Theory]
        [MemberData(nameof(IsDegree_Data))]
        public void HasDegree(Term term, int degree)
        {
            Assert.Equal(term.Degree == degree, term.HasDegree(degree));
        }

        public static IEnumerable<object[]> IsDegree_Data()
            => new[]
            {
                5,
                0,
                3 * X(),
                -2 * X(-1)
            }
            .SelectMany(term => Enumerable.Range(-2, count: 5)
                .Select(degree => new object[] { term, degree }));

        [Theory]
        [ClassData(typeof(Term_Data))]
        public void MatchConstant(Term term)
        {
            Assert.Equal(term.IsConstant, term.MatchConstant(out int constant));

            if (term.IsConstant)
            {
                Assert.Equal(constant, term); // Implicit conversion to Term
                Assert.Equal(constant, term.Coefficient);
            }
        }

        [Theory]
        [MemberData(nameof(Multiply_Term_Int32_Data))]
        public void Multiply_Term_Int32(Term term, int coefficient, Term expected)
        {
            Assert.Equal(expected, coefficient * term);
            Assert.Equal(expected, term * coefficient); // Multiplication should be commutative
        }

        public static IEnumerable<object[]> Multiply_Term_Int32_Data()
            => new[]
            {
                new object[] { (Term)5, -4, (Term)(-20) },
                new object[] { 3 * X(2), -5, -15 * X(2) },
                new object[] { -2 * X(-4), -10, 20 * X(-4) },
                new object[] { X(), 0, Term.Zero }
            };

        [Theory]
        [ClassData(typeof(Term_Term_Data))]
        public void Multiply_Term_Term(Term first, Term second)
        {
            void Verify(Term product)
            {
                Assert.Equal(first.Coefficient * second.Coefficient, product.Coefficient);
                int expectedDegree =
                    first == 0 || second == 0
                    ? 0 : first.Degree + second.Degree;
                Assert.Equal(expectedDegree, product.Degree);
            }

            Verify(first * second);
            Verify(second * first);
        }

        [Theory]
        [MemberData(nameof(Pow_Data))]
        public void Pow(Term term, int exponent, Term expected)
        {
            Assert.Equal(expected, term.Pow(exponent));
        }

        public static IEnumerable<object[]> Pow_Data()
            => new[]
            {
                new object[] { (Term)3, 4, (Term)81 },
                new object[] { 3 * X(4), 5, 243 * X(20) },
                new object[] { -3 * X(-1), 3, -27 * X(-3) },
                new object[] { 5 * X(5), 0, Term.One }
            };

        [Theory]
        [ClassData(typeof(Pow_Negative_Invalid_Data))]
        public void Pow_Negative_Invalid_Data(int exponent)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => X().Pow(exponent));
        }

        [Theory]
        [MemberData(nameof(WithCoefficient_Data))]
        public void WithCoefficient(Term term, int coefficient, Term expected)
        {
            Assert.Equal(expected, term.WithCoefficient(coefficient));
        }

        public static IEnumerable<object[]> WithCoefficient_Data()
            => new[]
            {
                new object[] { X(-1), 2, 2 * X(-1) },
                new object[] { Term.Zero, 2, (Term)2 },
                new object[] { 6 * X(), 6, 6 * X() },
                new object[] { 5 * X(5), 0, Term.Zero },
                new object[] { 5 * X(-5), 0, Term.Zero }
            };

        [Theory]
        [MemberData(nameof(WithDegree_Data))]
        public void WithDegree(Term term, int degree, Term expected)
        {
            Assert.Equal(expected, term.WithDegree(degree));
        }

        public static IEnumerable<object[]> WithDegree_Data()
            => new[]
            {
                new object[] { 5 * X(5), 1, 5 * X() },
                new object[] { 5 * X(5), 0, (Term)5 },
                new object[] { 5 * X(), 1, 5 * X() },
                new object[] { Term.Zero, 5, Term.Zero }
            };
    }
}
