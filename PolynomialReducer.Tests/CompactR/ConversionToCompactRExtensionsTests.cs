﻿using System;
using System.Collections.Generic;
using System.Text;
using PolynomialReducer.CompactR;
using PolynomialReducer.Nodes;
using PolynomialReducer.Tests.TestInternal;
using Xunit;
using static PolynomialReducer.CompactR.CompactRFactory;

namespace PolynomialReducer.Tests.CompactR
{
    public class ConversionToCompactRExtensionsTests
    {
        [Theory]
        [MemberData(nameof(ToEquality_Data))]
        public void ToEquality(EquationNode equation, Equality expected)
        {
            Assert.Equal(expected, equation.ToEquality());
        }

        public static MemberData<EquationNode, Equality> ToEquality_Data()
            => MemberData.Create(new[]
            {
                (Algebra.ParseEquation("(x + x^-1)^2 = -2"), (X(2) + 2 + X(-2)).EqualTo(-2)),
                (Algebra.ParseEquation("1 + (2x + (3x^2 + 4x^3)) = 4x^9"), (1 + 2 * X() + 3 * X(2) + 4 * X(3)).EqualTo(4 * X(9)))
            });

        [Theory]
        [MemberData(nameof(ToPolynomial_Expression_Data))]
        public void ToPolynomial_Expression(ExpressionNode expression, Polynomial expected)
        {
            Assert.Equal(expected, expression.ToPolynomial());
        }

        public static MemberData<ExpressionNode, Polynomial> ToPolynomial_Expression_Data()
            => MemberData.Create(new[]
            {
                (Algebra.ParseExpression(""))
            });

        [Theory]
        [MemberData(nameof(ToPolynomial_Factor_Data))]
        public void ToPolynomial_Factor(FactorNode factor, Polynomial expected)
        {
            Assert.Equal(expected, factor.ToPolynomial());
        }

        [Theory]
        [MemberData(nameof(ToPolynomial_NodeList_Factor_Data))]
        public void ToPolynomial_NodeList_Factor(NodeList<FactorNode> factors, Polynomial expected)
        {
            Assert.Equal(expected, factors.ToPolynomial());
        }

        [Theory]
        [MemberData(nameof(ToPolynomial_NodeList_Term_Data))]
        public void ToPolynomial_NodeList_Term(NodeList<TermNode> terms, Polynomial expected)
        {
            Assert.Equal(expected, terms.ToPolynomial());
        }

        [Theory]
        [MemberData(nameof(ToPolynomial_Term_Data))]
        public void ToPolynomial_Term(TermNode term, Polynomial expected)
        {
            Assert.Equal(expected, term.ToPolynomial());
        }
    }
}
