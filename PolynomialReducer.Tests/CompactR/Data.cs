﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PolynomialReducer.CompactR;
using PolynomialReducer.Tests.TestInternal;
using static PolynomialReducer.CompactR.CompactRFactory;

namespace PolynomialReducer.Tests.CompactR
{
    public class Int32_Data : ClassData<int>
    {
        public override IEnumerable<int> GetData() => Enumerable.Range(-5, count: 11);
    }

    public class Int32_NonZero_Data : ClassData<int>
    {
        public override IEnumerable<int> GetData() => new Int32_Data().GetData().Except(new[] { 0 });
    }

    public class Polynomial_Data : ClassData<Polynomial>
    {
        public override IEnumerable<Polynomial> GetData()
            => new[]
            {
                // TODO: Update if Square() is re-added.
                (X() + X(-1)).Pow(2),
                X(3) - 1,
                (2 * X() + 1).Pow(2)
            }
            .Concat(new Term_Data().GetData().Select(t => (Polynomial)t));
    }

    public class Pow_Negative_Invalid_Data : ClassData<int>
    {
        public override IEnumerable<int> GetData()
            => new[]
            {
                -1,
                -2,
                int.MinValue
            };
    }

    public class Term_Data : ClassData<Term>
    {
        public override IEnumerable<Term> GetData()
            => new[]
            {
                X(),
                3 * X(),
                4 * X(-1),
                -2 * X(2),
                -9 * X(-2),
                0 * X() // This is equivalent to (Term)0
            }
            .Concat(new Int32_Data().GetData().Select(i => (Term)i));
    }

    public class Term_Term_Data : ClassData<Term, Term>
    {
        public override IEnumerable<(Term, Term)> GetData()
            => from t1 in new Term_Data().GetData()
               from t2 in new Term_Data().GetData()
               select (t1, t2);
    }
}
