﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolynomialReducer.Tests.TestInternal
{
    // These classes have to be public, unfortunately, for subtypes to be public.
    public abstract class ClassData<T> : IEnumerable<object[]>
    {
        public abstract IEnumerable<T> GetData();

        IEnumerator<object[]> IEnumerable<object[]>.GetEnumerator()
        {
            foreach (T datum in GetData())
            {
                yield return new object[] { datum };
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => this.AsEnumerable().GetEnumerator();
    }

    public abstract class ClassData<T1, T2> : IEnumerable<object[]>
    {
        public abstract IEnumerable<(T1, T2)> GetData();

        IEnumerator<object[]> IEnumerable<object[]>.GetEnumerator()
        {
            foreach ((T1, T2) datum in GetData())
            {
                yield return new object[] { datum.Item1, datum.Item2 };
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => this.AsEnumerable().GetEnumerator();
    }
}
