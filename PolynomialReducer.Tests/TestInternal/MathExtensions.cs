﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PolynomialReducer.Tests.TestInternal
{
    internal static class MathExtensions
    {
        public static int Choose(this int n, int k)
        {
            double numerator = n.Factorial();
            double denominator = k.Factorial() * checked(n - k).Factorial();
            return checked((int)(numerator / denominator));
        }

        // Gets the multinomial coefficient. See https://en.wikipedia.org/wiki/Multinomial_theorem
        public static int Choose(this int n, params int[] ks)
        {
            Debug.Assert(ks.Sum() == n);

            checked
            {
                double result = n.Factorial();

                foreach (int k in ks)
                {
                    result /= k.Factorial();
                }

                return (int)result;
            }
        }

        public static int Pow(this int value, int exponent) => checked((int)Math.Pow(value, exponent));

        private static double Factorial(this int n)
        {
            Debug.Assert(n >= 0);

            double result = 1d;

            checked
            {
                for (int i = 2; i <= n; i++)
                {
                    result *= i;
                }
            }

            return result;
        }
    }
}
