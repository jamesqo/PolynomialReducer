﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolynomialReducer.Tests.TestInternal
{
    internal static class MemberData
    {
        public static MemberData<T> Create<T>(IEnumerable<T> data) => new MemberData<T>(data);

        public static MemberData<T1, T2> Create<T1, T2>(IEnumerable<(T1, T2)> data) => new MemberData<T1, T2>(data);
    }

    public class MemberData<T> : ClassData<T>
    {
        private readonly IEnumerable<T> _data;

        internal MemberData(IEnumerable<T> data) => _data = data;

        public override IEnumerable<T> GetData() => _data;
    }

    public class MemberData<T1, T2> : ClassData<T1, T2>
    {
        private readonly IEnumerable<(T1, T2)> _data;

        internal MemberData(IEnumerable<(T1, T2)> data) => _data = data;

        public override IEnumerable<(T1, T2)> GetData() => _data;
    }
}
