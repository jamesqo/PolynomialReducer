﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PolynomialReducer.CompactR;
using PolynomialReducer.Internal.Tokens;
using PolynomialReducer.Nodes;

namespace PolynomialReducer.Tests
{
    public partial class E2ETestCase
    {
        public static Builder CreateBuilder() => new Builder();

        public string InputText { get; private set; }

        public IEnumerable<AlgebraToken> Tokens { get; private set; }

        public EquationNode Equation { get; private set; }

        public string EquationToString { get; private set; }

        public EquationNode ShallowEquation { get; private set; }

        // TODO: {Equality,RationalEquality}ToString?
        public Equality Equality { get; private set; }

        public Type EqualityException { get; private set; }

        public RationalEquality RationalEquality { get; private set; }

        public Type RationalEqualityException { get; private set; }
    }
}
