﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PolynomialReducer.CompactR;
using PolynomialReducer.Internal.Tokens;
using PolynomialReducer.Nodes;
using static PolynomialReducer.CompactR.CompactRFactory;

namespace PolynomialReducer.Tests
{
    public partial class E2ETestCase
    {
        public class Builder
        {
            internal Builder()
            {
            }

            public static implicit operator E2ETestCase(Builder builder) => builder.Build();

            private string _inputText;

            public Builder InputText(string inputText)
            {
                _inputText = inputText;
                return this;
            }

            private IEnumerable<AlgebraToken> _tokens;

            public Builder Tokens(params AlgebraToken[] tokens)
            {
                Debug.Assert(tokens.Last().IsKind(TokenKind.Eof), "You forgot to put an EOF token at the end!");

                _tokens = tokens;
                return this;
            }

            private EquationNode _equation;

            public Builder Equation(EquationNode equation)
            {
                _equation = equation;
                return this;
            }

            private string _equationToString;

            public Builder EquationToString(string equationToString)
            {
                _equationToString = equationToString;
                return this;
            }

            private EquationNode _shallowEquation;

            public Builder ShallowEquation(EquationNode shallowEquation)
            {
                _shallowEquation = shallowEquation;
                return this;
            }

            private Equality _equality;

            public Builder Equality(Equality equality)
            {
                _equality = equality;
                return this;
            }

            private Type _equalityException;

            public Builder EqualityException<TException>()
                where TException : Exception
            {
                _equalityException = typeof(TException);
                return this;
            }

            private RationalEquality _rationalEquality;

            public Builder RationalEquality(RationalEquality rationalEquality)
            {
                _rationalEquality = rationalEquality;
                return this;
            }

            private Type _rationalEqualityException;

            public Builder RationalEqualityException<TException>()
                where TException : Exception
            {
                _rationalEqualityException = typeof(TException);
                return this;
            }

            private E2ETestCase Build()
            {
                Debug.Assert(
                    (_equality == null || _equalityException == null) &&
                    (_rationalEquality == null || _rationalEqualityException == null),
                    "Cannot expect a method to both return a value and throw an exception.");
                Debug.Assert(
                    _equality == null || _rationalEquality == null,
                    "There is no set the rational equality if you have set the equality.");

                return new E2ETestCase
                {
                    InputText = _inputText,
                    Tokens = _tokens,
                    Equation = _equation,
                    // Most test cases have formatted input, and ToString() is the same text in such cases.
                    EquationToString = _equationToString ?? _inputText,
                    // In many test cases, there are no nested fractions and flattening the equation yields the same equation.
                    ShallowEquation = _shallowEquation ?? _equation,
                    Equality = _equality,
                    // Often, conversion to RationalEquality but not Equality is possible because the equation isn't flat.
                    // If neither Equality nor EqualityException was called, assume an ArgumentException due to not being flat
                    // is raised when trying to convert to Equality.
                    EqualityException = _equalityException ?? (_equality == null ? typeof(ArgumentException) : null),
                    RationalEquality = _rationalEquality ?? (_equality != null ? ToRationalEquality(_equality) : null),
                    RationalEqualityException = _rationalEqualityException
                };
            }

            private static RationalEquality ToRationalEquality(Equality equality)
            {
                RationalSum ToRationalSum(Polynomial polynomial)
                    => polynomial.Aggregate(RationalSum.Zero, (acc, t) => acc + t);

                return ToRationalSum(equality.LeftSide).EqualTo(ToRationalSum(equality.RightSide));
            }
        }
    }
}
