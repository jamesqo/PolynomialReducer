﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PolynomialReducer.CompactR;
using PolynomialReducer.Tests.TestInternal;
using static PolynomialReducer.CompactR.CompactRFactory;
using static PolynomialReducer.Internal.Tokens.TokenFactory;
using static PolynomialReducer.Nodes.NodeFactory;

namespace PolynomialReducer.Tests
{
    public class E2EData_All : ClassData<E2ETestCase>
    {
        public override IEnumerable<E2ETestCase> GetData()
            => new IEnumerable<E2ETestCase>[]
            {
                new E2EData_CannotBeParsed().GetData(),
                new E2EData_CanBeParsed().GetData(),
            }
            .SelectMany(x => x);
    }

    /// <summary>
    /// Contains inputs that can be lexed, but cannot be parsed.
    /// </summary>
    public class E2EData_CannotBeParsed : ClassData<E2ETestCase>
    {
        public override IEnumerable<E2ETestCase> GetData()
        {
            // No '=' sign
            yield return E2ETestCase.CreateBuilder()
                .InputText("2x^2 + 5x")
                .Tokens(
                    Number("2"),
                    X(),
                    Hat(),
                    Number("2"),
                    Plus(),
                    Number("5"),
                    X(),
                    Eof());

            // '-' character at very end of string
            // Previously, to know whether to lex - as the subtraction operator or part of a negative number, the lexer
            // looked ahead one character to see if the next number was a digit. But it can't do that if the - is the
            // last character.
            yield return E2ETestCase.CreateBuilder()
                .InputText("x -")
                .Tokens(
                    X(),
                    Minus(),
                    Eof());

            // Exponentiation to non-constant power (x)
            yield return E2ETestCase.CreateBuilder()
                .InputText("3^x = 9")
                .Tokens(
                    Number("3"),
                    Hat(),
                    X(),
                    Equal(),
                    Number("9"),
                    Eof());

            // Exponentiation to non-constant power (parenthesized)
            yield return E2ETestCase.CreateBuilder()
                .InputText("3^(x + 2) = 9")
                .Tokens(
                    Number("3"),
                    Hat(),
                    Lparen(),
                    X(),
                    Plus(),
                    Number("2"),
                    Rparen(),
                    Equal(),
                    Number("9"),
                    Eof());
        }
    }

    public class E2EData_CanBeParsed : ClassData<E2ETestCase>
    {
        public override IEnumerable<E2ETestCase> GetData()
        {
            // TODO: Once TokenFactory.X() no longer clashes with Term.X(int degree = 1), remove all explicit 1s in this file.

            yield return E2ETestCase.CreateBuilder()
                .InputText("3 * x + 5 = 6")
                .Tokens(
                    Number("3"),
                    Star(),
                    X(),
                    Plus(),
                    Number("5"),
                    Equal(),
                    Number("6"),
                    Eof())
                .Equation(
                    Equation(
                        left: Expression(
                            Term(3, Factor(Variable())),
                            5),
                        right: 6))
                .Equality((3 * X(1) + 5).EqualTo(6));

            // Negative numbers
            yield return E2ETestCase.CreateBuilder()
                .InputText("3 * x^2 + 9 * x = -544")
                .Tokens(
                    Number("3"),
                    Star(),
                    X(),
                    Hat(),
                    Number("2"),
                    Plus(),
                    Number("9"),
                    Star(),
                    X(),
                    Equal(),
                    Minus(),
                    Number("544"),
                    Eof())
                .Equation(
                    Equation(
                        left: Expression(
                            Term(3, Factor(Variable(), 2)),
                            Term(9, Factor(Variable()))),
                        right: -544))
                .Equality((3 * X(2) + 9 * X(1)).EqualTo(-544));

            // Parenthesized nodes
            // Converting to {Rational}Equality throws OverflowException
            yield return E2ETestCase.CreateBuilder()
                .InputText("(x) = ( ( 9 * 10 ^ 9) ) ")
                .Tokens(
                    Lparen(),
                    X(),
                    Rparen(),
                    Equal(),
                    Lparen(),
                    Lparen(),
                    Number("9"),
                    Star(),
                    Number("10"),
                    Hat(),
                    Number("9"),
                    Rparen(),
                    Rparen(),
                    Eof())
                .Equation(
                    Equation(
                        left: Expression(Term(Factor(Parenthesized(
                            Expression(Term(Factor(Variable()))))))),
                        right: Expression(Term(Factor(Parenthesized(
                            Expression(Term(Factor(Parenthesized(
                                Expression(Term(
                                    9,
                                    Factor(10, 9)))))))))))))
                .EquationToString("(x) = ((9 * 10^9))")
                .EqualityException<OverflowException>()
                .RationalEqualityException<OverflowException>();

            // Exponentiation of parenthesized nodes
            yield return E2ETestCase.CreateBuilder()
                .InputText("(x - 2)^4 = 16")
                .Tokens(
                    Lparen(),
                    X(),
                    Minus(),
                    Number("2"),
                    Rparen(),
                    Hat(),
                    Number("4"),
                    Equal(),
                    Number("16"),
                    Eof())
                .Equation(
                    Equation(
                        left: Expression(Term(Factor(
                            Parenthesized(Expression(
                                Term(Factor(Variable())),
                                -Term(2))),
                            exponent: 4))),
                        right: 16))
                // Binomial expansion of (x - 2)^4 on lhs
                .Equality(
                    (X(4)
                    - 4.Choose(1) * X(3) * 2
                    + 4.Choose(2) * X(2) * 2.Pow(2)
                    - 4.Choose(3) * X(1) * 2.Pow(3)
                    + 2.Pow(4)).EqualTo(16));

            // Division
            yield return E2ETestCase.CreateBuilder()
                .InputText("3* 5/ 4 + 6 = 1 /1 /1 * 8 / 9 * 6")
                .Tokens(
                    Number("3"),
                    Star(),
                    Number("5"),
                    Slash(),
                    Number("4"),
                    Plus(),
                    Number("6"),
                    Equal(),
                    Number("1"),
                    Slash(),
                    Number("1"),
                    Slash(),
                    Number("1"),
                    Star(),
                    Number("8"),
                    Slash(),
                    Number("9"),
                    Star(),
                    Number("6"),
                    Eof())
                .Equation(
                    Equation(
                        left: Expression(
                            Term(
                                Numerator(3, 5),
                                Denominator(4)),
                            6),
                        right: Expression(
                            Term(
                                Numerator(1, 8, 6),
                                Denominator(1, 1, 9)))))
                .EquationToString("3 * 5 / 4 + 6 = 1 * 8 * 6 / 1 / 1 / 9")
                .RationalEquality(
                    (Q(3 * 5) / 4 + 6).EqualTo(Q(1 * 8 * 6) / 1 / 1 / 9));

            // Your typical rational equation
            yield return E2ETestCase.CreateBuilder()
                .InputText("x / (x^3 + 7) = (x^2 + 4) / (x^5 + 1)")
                .Tokens(
                    X(),
                    Slash(),
                    Lparen(),
                    X(),
                    Hat(),
                    Number("3"),
                    Plus(),
                    Number("7"),
                    Rparen(),
                    Equal(),
                    Lparen(),
                    X(),
                    Hat(),
                    Number("2"),
                    Plus(),
                    Number("4"),
                    Rparen(),
                    Slash(),
                    Lparen(),
                    X(),
                    Hat(),
                    Number("5"),
                    Plus(),
                    Number("1"),
                    Rparen(),
                    Eof())
                .Equation(
                    Equation(
                        left: Expression(Term(
                            Numerator(Factor(Variable())),
                            Denominator(
                                Factor(Parenthesized(Expression(
                                    Term(Factor(Variable(), 3)),
                                    7)))))),
                        right: Expression(Term(
                            Numerator(
                                Factor(Parenthesized(Expression(
                                    Term(Factor(Variable(), 2)),
                                    4)))),
                            Denominator(
                                Factor(Parenthesized(Expression(
                                    Term(Factor(Variable(), 5)),
                                    1))))))))
                .RationalEquality(
                    (X(1) / (X(3) + 7)).EqualTo((X(2) + 4) / (X(5) + 1)));

            // An equation with nested fractions that needs to be balanced
            yield return E2ETestCase.CreateBuilder()
                .InputText("x * (1 + 1 / x) / x^2 / (9 + x / x) = 1")
                .Tokens(
                    X(),
                    Star(),
                    Lparen(),
                    Number("1"),
                    Plus(),
                    Number("1"),
                    Slash(),
                    X(),
                    Rparen(),
                    Slash(),
                    X(),
                    Hat(),
                    Number("2"),
                    Slash(),
                    Lparen(),
                    Number("9"),
                    Plus(),
                    X(),
                    Slash(),
                    X(),
                    Rparen(),
                    Equal(),
                    Number("1"),
                    Eof())
                .Equation(
                    Equation(
                        left: Expression(Term(
                            Numerator(
                                Factor(Variable()),
                                Factor(Parenthesized(Expression(
                                    1,
                                    Term(
                                        Numerator(1),
                                        Denominator(Factor(Variable()))))))),
                            Denominator(
                                Factor(Variable(), 2),
                                Factor(Parenthesized(Expression(
                                    9,
                                    Term(
                                        Numerator(Factor(Variable())),
                                        Denominator(Factor(Variable()))))))))),
                        right: 1))
                // x * (1 * x + 1) * x / x^2 / (9 * x + x) / x = 1
                .ShallowEquation(
                    Equation(
                        left: Expression(Term(
                            Numerator(
                                Factor(Variable()),
                                Factor(Parenthesized(Expression(
                                    Term(1, Factor(Variable())),
                                    1))),
                                Factor(Variable())),
                            Denominator(
                                Factor(Variable(), 2),
                                Factor(Parenthesized(Expression(
                                    Term(9, Factor(Variable())),
                                    Term(Factor(Variable()))))),
                                Factor(Variable())))),
                        right: 1))
                .RationalEquality(((X(3) + X(2)) / (10 * X(4))).EqualTo(1));

            // Triple (or more) nested fractions
            yield return E2ETestCase.CreateBuilder()
                .InputText("1 + 2 / (3 + 4 / (5 + 6 / (7 + 8))) = x")
                .Tokens(
                    Number("1"),
                    Plus(),
                    Number("2"),
                    Slash(),
                    Lparen(),
                    Number("3"),
                    Plus(),
                    Number("4"),
                    Slash(),
                    Lparen(),
                    Number("5"),
                    Plus(),
                    Number("6"),
                    Slash(),
                    Lparen(),
                    Number("7"),
                    Plus(),
                    Number("8"),
                    Rparen(),
                    Rparen(),
                    Rparen(),
                    Equal(),
                    X(),
                    Eof())
                .Equation(
                    Equation(
                        left: Expression(1, Term(
                            Numerator(2),
                            Denominator(Factor(Parenthesized(Expression(3, Term(
                                Numerator(4),
                                Denominator(Factor(Parenthesized(Expression(5, Term(
                                    Numerator(6),
                                    Denominator(Factor(Parenthesized(Expression(7, 8)))))))))))))))),
                        right: Expression(Term(Factor(Variable())))))
                // 1 + 2 * (5 * (7 + 8) + 6) / (3 * (5 * (7 + 8) + 6) + 4 * (7 + 8)) = x
                .ShallowEquation(
                    Equation(
                        left: Expression(1, Term(
                            Numerator(
                                2,
                                Factor(Parenthesized(Expression(
                                    Term(5, Factor(Parenthesized(Expression(
                                        7, 8)))),
                                    6)))),
                            Denominator(
                                Factor(Parenthesized(Expression(
                                    Term(3, Factor(Parenthesized(Expression(
                                        Term(5, Factor(Parenthesized(Expression(
                                            7, 8)))),
                                        6)))),
                                    Term(4, Factor(Parenthesized(Expression(
                                        7, 8)))))))))),
                        right: Expression(Term(Factor(Variable())))))
                .RationalEquality(
                    (1 + Q(2 * (5 * (7 + 8) + 6)) / ((3 * (5 * (7 + 8) + 6) + 4 * (7 + 8)))).EqualTo(X(1)));

            // Denominator directly contains nested parentheses, which contain fractions after 2+ levels of nesting
            // (The next parenthesis is at the same level as the first, not in a nested denominator)

            // The equation below has a shallow equation whose AST is too big to construct manually, so just build the
            // equivalent string and hand it to the frontend to parse.
            string subexpr_x = "x";
            string subexpr_9_10_11 = $"(9 * {subexpr_x} + (10 * {subexpr_x} + 11))";
            string subexpr_6_7_8 = $"(6 * {subexpr_9_10_11} + (7 * {subexpr_9_10_11} + 8 * {subexpr_x}))";
            string subexpr_3_4_5 = $"(3 * {subexpr_6_7_8} + (4 * {subexpr_6_7_8} + 5 * {subexpr_9_10_11}))";
            var shallowEquation = Algebra.ParseEquation($"1 + 2 * {subexpr_6_7_8} / {subexpr_3_4_5} = 1");

            // TODO: Add a WithRationalEquality test for this?
            yield return E2ETestCase.CreateBuilder()
                .InputText("1 + 2 / (3 + (4 + 5 / (6 + (7 + 8 / (9 + (10 + 11 / x)))))) = 1")
                .Tokens(
                    Number("1"),
                    Plus(),
                    Number("2"),
                    Slash(),
                    Lparen(),
                    Number("3"),
                    Plus(),
                    Lparen(),
                    Number("4"),
                    Plus(),
                    Number("5"),
                    Slash(),
                    Lparen(),
                    Number("6"),
                    Plus(),
                    Lparen(),
                    Number("7"),
                    Plus(),
                    Number("8"),
                    Slash(),
                    Lparen(),
                    Number("9"),
                    Plus(),
                    Lparen(),
                    Number("10"),
                    Plus(),
                    Number("11"),
                    Slash(),
                    X(),
                    Rparen(),
                    Rparen(),
                    Rparen(),
                    Rparen(),
                    Rparen(),
                    Rparen(),
                    Equal(),
                    Number("1"),
                    Eof())
                .Equation(
                    Equation(
                        left: Expression(1, Term(
                            Numerator(2),
                            Denominator(Factor(Parenthesized(Expression(
                                3,
                                Term(Factor(Parenthesized(Expression(4, Term(
                                    Numerator(5),
                                    Denominator(Factor(Parenthesized(Expression(
                                        6,
                                        Term(Factor(Parenthesized(Expression(7, Term(
                                            Numerator(8),
                                            Denominator(Factor(Parenthesized(Expression(
                                                9,
                                                Term(Factor(Parenthesized(Expression(10, Term(
                                                    Numerator(11),
                                                    Denominator(Factor(Variable()))))))))))))))))))))))))))))))),
                        right: 1))
                .ShallowEquation(shallowEquation);

            // No-star multiplication, constant + x
            yield return E2ETestCase.CreateBuilder()
                .InputText("2x + 6 = 9")
                .Tokens(
                    Number("2"),
                    X(),
                    Plus(),
                    Number("6"),
                    Equal(),
                    Number("9"),
                    Eof())
                .Equation(
                    Equation(
                        left: Expression(Term(2, Factor(Variable())), 6),
                        right: 9))
                .EquationToString("2 * x + 6 = 9")
                .Equality((2 * X(1) + 6).EqualTo(9));

            // No-star multiplication, parenthesized + parenthesized
            yield return E2ETestCase.CreateBuilder()
                .InputText("(x - 2)(x - 4) = 8")
                .Tokens(
                    Lparen(),
                    X(),
                    Minus(),
                    Number("2"),
                    Rparen(),
                    Lparen(),
                    X(),
                    Minus(),
                    Number("4"),
                    Rparen(),
                    Equal(),
                    Number("8"),
                    Eof())
                .Equation(
                    Equation(
                        left: Expression(Term(
                            Factor(Parenthesized(Expression(
                                Term(Factor(Variable())),
                                -Term(2)))),
                            Factor(Parenthesized(Expression(
                                Term(Factor(Variable())),
                                -Term(4)))))),
                        right: 8))
                .EquationToString("(x - 2) * (x - 4) = 8")
                .Equality((X(2) - 6 * X(1) + 8).EqualTo(8));

            // No-star multiplication has higher precedence than division
            yield return E2ETestCase.CreateBuilder()
                .InputText("3 / 2x = 9")
                .Tokens(
                    Number("3"),
                    Slash(),
                    Number("2"),
                    X(),
                    Equal(),
                    Number("9"),
                    Eof())
                .Equation(
                    Equation(
                        left: Expression(Term(
                            Numerator(3),
                            Denominator(2, Factor(Variable())))),
                        right: 9))
                .EquationToString("3 / 2 / x = 9")
                .RationalEquality((3 / (2 * X(1))).EqualTo(9));

            // Exponentiation has higher precedence than no-star multiplication
            yield return E2ETestCase.CreateBuilder()
                .InputText("3^6x = 1")
                .Tokens(
                    Number("3"),
                    Hat(),
                    Number("6"),
                    X(),
                    Equal(),
                    Number("1"),
                    Eof())
                .Equation(
                    Equation(
                        left: Expression(Term(
                            Factor(3, 6),
                            Factor(Variable()))),
                        right: 1))
                .EquationToString("3^6 * x = 1")
                .Equality((3.Pow(6) * X(1)).EqualTo(1));

            // Whether a - is the subtraction operator or part of a negative number should be
            // determined by the parser, not the lexer.
            // Explanation: The lexer used to determine what a - represented by checking the
            // next character to see if it was a digit. If so, it lexed the - as part of a number
            // token. When no-star multiplcation was introduced, this made inputs such as 6-3
            // semantically valid and parse as 6 * -3 rather than 6 - 3.
            yield return E2ETestCase.CreateBuilder()
                .InputText("6-3 = 5")
                .Tokens(
                    Number("6"),
                    Minus(),
                    Number("3"),
                    Equal(),
                    Number("5"),
                    Eof())
                .Equation(
                    // TODO: We shouldn't have to write both WithEquation() and Equation() every time.
                    Equation(
                        left: Expression(6, -Term(3)),
                        right: 5))
                .EquationToString("6 - 3 = 5")
                .Equality(((Term)3).EqualTo(5));

            // Exponentiation to negative power
            yield return E2ETestCase.CreateBuilder()
                .InputText("x^-2 = 1 / 16")
                .Tokens(
                    X(),
                    Hat(),
                    Minus(),
                    Number("2"),
                    Equal(),
                    Number("1"),
                    Slash(),
                    Number("16"),
                    Eof())
                .Equation(
                    Equation(
                        left: Expression(Term(Factor(Variable(), -2))),
                        right: Expression(Term(
                            Numerator(1),
                            Denominator(16)))))
                .RationalEquality(Q(X(-2)).EqualTo(Q(1) / 16));
        }
    }
}
