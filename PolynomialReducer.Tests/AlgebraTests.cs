﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace PolynomialReducer.Tests
{
    public class AlgebraTests
    {
        [Theory]
        [ClassData(typeof(E2EData_CanBeParsed))]
        public void ParseEquation(E2ETestCase testCase)
        {
            Assert.Equal(testCase.Equation, Algebra.ParseEquation(testCase.InputText));
        }
    }
}
