﻿using System;
using System.Collections.Generic;
using System.Text;
using PolynomialReducer.Internal.Nodes;
using Xunit;

namespace PolynomialReducer.Tests.Internal.Nodes
{
    public class MakeShallowExtensionsTests
    {
        [Theory]
        [ClassData(typeof(E2EData_CanBeParsed))]
        public void MakeShallow(E2ETestCase testCase)
        {
            Assert.Equal(testCase.ShallowEquation, testCase.Equation.MakeShallow());
        }
    }
}
