﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PolynomialReducer.Internal.FrontEnd;
using Xunit;

namespace PolynomialReducer.Tests.Internal.FrontEnd
{
    public class AlgebraParserTests
    {
        // TODO: Consider re-enabling for all inputs if the parser finds a way to deal with invalid token sequences.
        [Theory]
        [ClassData(typeof(E2EData_CanBeParsed))]
        public void ReadEquation_FromInputText(E2ETestCase testCase)
        {
            var parser = new AlgebraParser(testCase.InputText);
            var actual = parser.ReadEquation();
            Assert.Equal(testCase.Equation, actual);
        }

        [Theory]
        [ClassData(typeof(E2EData_CanBeParsed))]
        public void ReadEquation_FromTokens(E2ETestCase testCase)
        {
            var parser = new AlgebraParser(testCase.Tokens);
            var actual = parser.ReadEquation();
            Assert.Equal(testCase.Equation, actual);
            // TODO: Consider adding a ReadAll() method to the parser that returns an IEnumerable<AstNode>
            // for more thorough testing, e.g. making sure it handles EOF properly?
        }

        [Theory]
        [ClassData(typeof(E2EData_CannotBeParsed))]
        public void ReadEquation_FromInputText_Invalid(E2ETestCase testCase)
        {
            var parser = new AlgebraParser(testCase.InputText);
            Assert.Throws<ParserException>(() => parser.ReadEquation());
        }

        [Theory]
        [ClassData(typeof(E2EData_CannotBeParsed))]
        public void ReadEquation_FromTokens_Invalid(E2ETestCase testCase)
        {
            var parser = new AlgebraParser(testCase.Tokens);
            Assert.Throws<ParserException>(() => parser.ReadEquation());
        }
    }
}
