﻿using System;
using System.Collections.Generic;
using System.Text;
using PolynomialReducer.Internal.FrontEnd;
using Xunit;

namespace PolynomialReducer.Tests.Internal.FrontEnd
{
    public class AlgebraLexerTests
    {
        [Theory]
        [ClassData(typeof(E2EData_All))]
        public void LexAll(E2ETestCase testCase)
        {
            var lexer = new AlgebraLexer(testCase.InputText);
            var actual = lexer.LexAll();
            Assert.Equal(testCase.Tokens, actual);
        }
    }
}
